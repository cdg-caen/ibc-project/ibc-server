package com.cegesoft.ibc.server.event.event;

import com.cegesoft.ibc.server.event.Event;

public class SerialSendEvent extends Event {

    private final String message;

    public SerialSendEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
