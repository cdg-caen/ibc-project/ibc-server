package com.cegesoft.ibc.server.connection.socket;

import com.cegesoft.ibc.server.message.Message;

import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class SocketConnection implements Closeable {

    private Socket socket;
    private DataOutputStream writer;
    private DataInputStream reader;

    SocketConnection(Socket socket) {
        this.socket = socket;
        this.initStreams();
    }

    private void initStreams() {
        try {
            writer = new DataOutputStream(socket.getOutputStream());
            reader = new DataInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String message) throws IOException {
        if (writer == null)
            return;
        writer.writeUTF(message);
    }

    public void sendMessage(Message message) throws IOException {
        this.sendMessage(message.parse());
    }

    public String waitForAnswer() throws IOException {
        if (reader == null)
            return "";
        return reader.readUTF();
    }

    @Override
    public void close() throws IOException {
        writer.close();
        reader.close();
        socket.close();
    }
}
