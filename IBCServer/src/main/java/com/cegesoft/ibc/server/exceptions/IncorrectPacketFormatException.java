package com.cegesoft.ibc.server.exceptions;

public class IncorrectPacketFormatException extends Exception {

    public IncorrectPacketFormatException(String packet){
        super("IBCPacket incorrectly formatted : " + packet);
    }

    public IncorrectPacketFormatException(String packet, Throwable thr){
        super("IBCPacket incorrectly formatted : " + packet, thr);
    }

}
