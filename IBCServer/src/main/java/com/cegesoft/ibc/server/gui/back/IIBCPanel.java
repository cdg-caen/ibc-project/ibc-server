package com.cegesoft.ibc.server.gui.back;

import java.awt.*;
import java.awt.event.KeyEvent;

public interface IIBCPanel {

    void paintComponents(Graphics g, IBCPanel panel);

    void onKeyPressed(KeyEvent e, IBCPanel panel);

    void onLoad(IBCPanel panel);

    void unload(IBCPanel panel);

    boolean isLoad();

    Component[] getComponents();

}
