package com.cegesoft.ibc.server.event.event;

import com.cegesoft.ibc.server.event.Event;

public class TokenCreateEvent extends Event {
    private final String token;

    public TokenCreateEvent(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
