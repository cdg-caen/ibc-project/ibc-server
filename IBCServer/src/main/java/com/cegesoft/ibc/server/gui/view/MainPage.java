package com.cegesoft.ibc.server.gui.view;

import com.cegesoft.ibc.server.ServerApplication;
import com.cegesoft.ibc.server.gui.IGUIPage;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;

import java.net.URL;
import java.util.ResourceBundle;

public class MainPage implements IGUIPage, Initializable {

    public Pane pane;
    public Label title;
    public Button demoButton;
    public AnchorPane content;
    public Button describedButton;
    public Button catButton;
    public Button slowButton;

    public void onDemoClick(MouseEvent mouseEvent) {
        ServerApplication.getServer().navigate(ProcessPage.class, "demo", "normal");
    }

    @Override
    public URL getStageURL() {
        return getClass().getResource("/view/MainPage.fxml");
    }

    @Override
    public void disable() {

    }

    @Override
    public void enable(String... args) {
        content.setMaxSize(ServerApplication.WIDTH, ServerApplication.HEIGHT);
        content.setMinSize(ServerApplication.WIDTH, ServerApplication.HEIGHT);
        content.setPrefSize(ServerApplication.WIDTH, ServerApplication.HEIGHT);

        pane.setMaxSize(ServerApplication.WIDTH, ServerApplication.HEIGHT);
        pane.setMinSize(ServerApplication.WIDTH, ServerApplication.HEIGHT);
        pane.setPrefSize(ServerApplication.WIDTH, ServerApplication.HEIGHT);

        title.setFont(Font.loadFont("file:resources/bauhaus.ttf", 90));
    }

    @Override
    public void resetGUI() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void onDescribedClick(MouseEvent mouseEvent) {
        ServerApplication.getServer().navigate(ProcessPage.class, "described", "normal");
    }

    public void onCatClick(MouseEvent mouseEvent) {
        ServerApplication.getServer().navigate(CatPage.class);
    }

    public void onSlowClick(MouseEvent mouseEvent) {
        ServerApplication.getServer().navigate(ProcessPage.class, "demo", "slow");
    }
}
