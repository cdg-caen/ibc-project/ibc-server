package com.cegesoft.ibc.server.utils;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public class SoundUtil {
    public static void tone(int hz, int msecs, int volume, boolean addHarmonic) throws LineUnavailableException {
        float frequency = 44100.0f;
        byte[] buf = new byte[addHarmonic ? 2 : 1];
        AudioFormat af = new AudioFormat(frequency, 8, buf.length, true, false);
        SourceDataLine sdl = AudioSystem.getSourceDataLine(af);
        sdl.open(af);
        sdl.start();
        for (int i = 0; i < msecs*frequency/1000; i++) {
            double angle = i/(frequency/hz)*2.0*Math.PI;
            buf[0] = (byte) (Math.sin(angle)*volume);
            if (addHarmonic) {
                buf[1] = (byte) (Math.sin(2*angle)*volume*0.6);
            }
            sdl.write(buf, 0, buf.length);
        }
        sdl.drain();
        sdl.stop();
        sdl.close();
    }

    public static void tone(int[] hz, int[] msecs, int volume, boolean addHarmonic) throws LineUnavailableException {
        float frequency = 44100.0f;
        byte[] buf = new byte[addHarmonic ? 2 : 1];
        AudioFormat af = new AudioFormat(frequency, 8, buf.length, true, false);
        SourceDataLine sdl = AudioSystem.getSourceDataLine(af);
        sdl.open(af);
        sdl.start();
        for (int l = 0; l < hz.length; l++) {
            for (int i = 0; i < msecs[l] * frequency / 1000; i++) {
                double angle = i / (frequency / hz[l]) * 2.0 * Math.PI;
                buf[0] = (byte) (Math.sin(angle) * volume);
                if (addHarmonic) {
                    buf[1] = (byte) (Math.sin(2 * angle) * volume * 0.6);
                }
                sdl.write(buf, 0, buf.length);
            }
        }
        sdl.drain();
        sdl.stop();
        sdl.close();
    }
}
