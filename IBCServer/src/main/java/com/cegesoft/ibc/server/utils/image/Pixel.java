package com.cegesoft.ibc.server.utils.image;

public class Pixel {

    private final int red;
    private final int green;
    private final int blue;
    private final int alpha;
    private final int i;

    Pixel(int i, int red, int green, int blue, int alpha) {
        this.i = i;
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.alpha = alpha;
    }

    public int getRed() {
        return red;
    }

    public int getGreen() {
        return green;
    }

    public int getBlue() {
        return blue;
    }

    public int getAlpha() {
        return alpha;
    }

    public int getI() {
        return i;
    }

    @Override
    public String toString() {
        return Integer.toString(-i);
    }
}
