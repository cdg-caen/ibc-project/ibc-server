package com.cegesoft.ibc.server.utils.image;

import javafx.geometry.Dimension2D;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class ImageDecomposer {

    private final HashMap<Location, Pixel> pixels = new LinkedHashMap<>();

    private final BufferedImage baseImage;
    private final int width;
    private final int height;
    private int current = 0;


    public ImageDecomposer(BufferedImage baseImage) {
        this.baseImage = baseImage;
        this.width = baseImage.getWidth();
        this.height = baseImage.getHeight();
        this.readImage();
    }

    private void readImage() {
        for (int j = 0; j < this.width; j++) {
            for (int i = 0; i < this.height; i++) {
                int pixel = this.baseImage.getRGB(j, i);
                int alpha = (pixel >> 24) & 0xff;
                int red = (pixel >> 16) & 0xff;
                int green = (pixel >> 8) & 0xff;
                int blue = (pixel) & 0xff;
                if (red >= 252 && blue >= 252 && green >= 252)
                    continue;
                pixels.put(new Location(j, i), new Pixel(pixel, red, green, blue, alpha));
            }
        }
        ;
    }

    public HashMap<Location, Pixel> getPixels() {
        return pixels;
    }

    public BufferedImage getBaseImage() {
        return baseImage;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public PixelPacket getAndNext() {
        Map.Entry<Location, Pixel> returner = new ArrayList<>(this.pixels.entrySet()).get(current);
        current++;
        if (current >= this.pixels.entrySet().size())
            current = 0;
        return new PixelPacket(returner.getValue(), returner.getKey(), new Dimension2D(this.width, this.height));
    }

    public Pixel get(int x, int y) {
        return this.pixels.get(new Location(x, y));
    }

    public Pixel get(Location location) {
        return this.pixels.entrySet().stream().filter(entry -> entry.getKey().equals(location)).map(Map.Entry::getValue).findFirst().orElse(null);
    }
}
