package com.cegesoft.ibc.server.process;

public abstract class ProcessHandler extends Thread {

    protected boolean interrupted;

    ProcessHandler() {
        this.setName("PROCESS-Thread");
        this.start();
    }

    protected abstract void execute();

    protected abstract void interruptProcess();

    @Override
    public void run() {
        interrupted = false;
        execute();
        super.run();
    }

    @Override
    public void interrupt() {
        interrupted = true;
        interruptProcess();
        super.interrupt();
    }
}
