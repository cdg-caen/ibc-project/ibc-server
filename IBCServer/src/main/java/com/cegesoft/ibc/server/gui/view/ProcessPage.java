package com.cegesoft.ibc.server.gui.view;

import com.cegesoft.ibc.server.ServerApplication;
import com.cegesoft.ibc.server.connection.serial.SerialService;
import com.cegesoft.ibc.server.event.EventHandler;
import com.cegesoft.ibc.server.event.event.*;
import com.cegesoft.ibc.server.gui.IGUIPage;
import com.cegesoft.ibc.server.gui.view.load.LoadAnimation;
import com.cegesoft.ibc.server.gui.view.load.LoadState;
import com.cegesoft.ibc.server.gui.view.points.Point;
import com.cegesoft.ibc.server.logging.IBCLogger;
import com.cegesoft.ibc.server.process.DefaultProcessHandler;
import com.cegesoft.ibc.server.service.ServiceManager;
import javafx.animation.*;
import javafx.application.Platform;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class ProcessPage implements IGUIPage, Initializable {

    private String legendText = "Posez une partie du corps sur le capteur";

    private static final String EVENT_TAG = "DefaultProcessTag";

    public AnchorPane content;
    public Pane pane;
    public Label title;
    public ImageView finger;
    public Label legend;
    public Pane infosRect;

    private Timeline fingerAnimation;
    public boolean demo = false;
    public boolean slow = false;
    private double widthOffSet = 0;
    @Override
    public URL getStageURL() {
        return getClass().getResource("/view/ProcessPage.fxml");
    }

    @Override
    public String getStyleSheets() {
        return "style/process.css";
    }

    @Override
    public void resetGUI() {

    }

    @Override
    public void onKeyPressed(KeyEvent event) {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @Override
    public void disable() {
        ServerApplication.getServer().stopProcess();
        stopAnimation();
    }

    @Override
    public void enable(String... args) {
        demo = args[0].equals("demo");
        slow = args[1].equals("slow");
        startAnimation();
        EventHandler.registerListener(StateEvent.SendResult.class, EVENT_TAG, event -> {
            ServerApplication.getServer().sendResults(event.getUser());
            EventHandler.unregisterListeners(EVENT_TAG);
        });
        if (slow) {
            legendText = "(Mode Bas Débit) " + legendText;
            legend.setText(legendText);
        }
        if (!demo) {
            widthOffSet = 300;
            title.setPrefWidth(title.getPrefWidth() - this.widthOffSet);
            title.setStyle("-fx-font: 80px \"Bauhaus 93\"; -fx-effect: dropshadow(two-pass-box, black, 10, .3, .3, .3);");
            Timeline timeline = new Timeline();
            List<KeyValue> frames = new ArrayList<>();
            for (Node node : pane.getChildren()) {
                frames.add(new KeyValue(node.layoutXProperty(), node.getLayoutX() + (node == title ? this.widthOffSet * 1.5 : (node == infosRect ? this.widthOffSet*2 : this.widthOffSet))));
            }
            timeline.getKeyFrames().add(new KeyFrame(new Duration(300), action -> this.registerSteps(), frames.toArray(new KeyValue[0])));
            timeline.play();
            return;
        }
        ServerApplication.getServer().startProcess(new DefaultProcessHandler(false, slow));
    }

    private void registerSteps() {
        double y = 0;
        final String bluetoothStartTitle = "En attente de Connexion";
        Point startPoint = new Point(LoadState.LOADING, "Démarrage de l'Application", 0, y, false);
        Point tokenPoint = new Point(LoadState.LOADING, "Création du Token", 0, y, true);
        Point bluetoothStartPoint = new Point(LoadState.LOADING, bluetoothStartTitle, 0, y, false);
        Point serialSendPoint = new Point(LoadState.LOADING, "Envoi des données vers le Capteur", 0, y, true);
        Point bluetoothReveivePoint = new Point(LoadState.LOADING, "Reception du Bluetooth", 0, y, true);
        Point socketSendPoint = new Point(LoadState.LOADING, "Envoi des données au Serveur", 0, y, true);
        Point socketReceivePoint = new Point(LoadState.LOADING, "Reception des données", 0, y, true);
        Point userFoundPoint = new Point(LoadState.LOADING, "Récupération des données de \nl'utilisateur", 0, y, true);
        Point sendResultPoint = new Point(LoadState.LOADING, "Affichage du résultat", 0, y, false);



        List<Point> points = Arrays.asList(
                startPoint,
                tokenPoint,
                serialSendPoint,
                bluetoothStartPoint,
                bluetoothReveivePoint,
                socketSendPoint,
                socketReceivePoint,
                userFoundPoint,
                sendResultPoint
        );

        for (Point point : points) {
            point.setY(y);
            point.setLayoutY(y);
            infosRect.getChildren().add(point);
            y += point.getPrefHeight() + 20;
        }

        startPoint.showAnimation();

        EventHandler.registerListener(StateEvent.Start.class, EVENT_TAG, event -> {
            startPoint.setState(LoadState.SUCCESS);
            tokenPoint.showAnimation();
        });

        EventHandler.registerListener(TokenCreateEvent.class, EVENT_TAG, event -> {
            tokenPoint.setState(LoadState.SUCCESS);
            tokenPoint.setDescription("Token : " + event.getToken());
            serialSendPoint.showAnimation();
        });

        EventHandler.registerListener(SerialSendEvent.class, EVENT_TAG, event -> {
            serialSendPoint.setDescription("Envoi de : " + format(event.getMessage()));
            bluetoothStartPoint.showAnimation();
        });

        EventHandler.registerListener(BluetoothServiceEvent.Connect.Post.class, EVENT_TAG, event -> {
            bluetoothStartPoint.setText("Bluetooth Connecté");
            bluetoothStartPoint.setState(LoadState.SUCCESS);
            bluetoothReveivePoint.showAnimation();
        });

        EventHandler.registerListener(BluetoothServiceEvent.Connect.Fail.class, EVENT_TAG, event -> {
            bluetoothStartPoint.setText("Erreur de Connexion");
            bluetoothStartPoint.setState(LoadState.ERROR);
        });

        EventHandler.registerListener(BluetoothServiceEvent.Receive.class, EVENT_TAG, event -> {
            serialSendPoint.setState(LoadState.SUCCESS);
            bluetoothReveivePoint.setState(LoadState.SUCCESS);
            bluetoothReveivePoint.setDescription("Reçu : " + format(event.getReceived()));
        });

        EventHandler.registerListener(WrongTokenReceiveEvent.class, EVENT_TAG, event -> {
            serialSendPoint.setState(LoadState.SUCCESS);
            bluetoothReveivePoint.setState(LoadState.ERROR);
            bluetoothReveivePoint.setDescription("Token Incorrect.");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            serialSendPoint.setState(LoadState.LOADING);
            bluetoothReveivePoint.setState(LoadState.LOADING, false);
            bluetoothReveivePoint.setDescription("");
            bluetoothStartPoint.setText(bluetoothStartTitle);
            bluetoothStartPoint.setState(LoadState.LOADING);
        });

        EventHandler.registerListener(SocketServiceEvent.Send.Pre.class, EVENT_TAG, event -> {
            socketSendPoint.showAnimation();
        });

        EventHandler.registerListener(SocketServiceEvent.Send.Post.class, EVENT_TAG, event -> {
            socketSendPoint.setState(LoadState.SUCCESS);
            socketSendPoint.setDescription("Envoi de : " + format(event.getMessage().parse()));
            socketReceivePoint.showAnimation();
        });

        EventHandler.registerListener(SocketServiceEvent.Receive.class, EVENT_TAG, event -> {
            socketReceivePoint.setState(LoadState.SUCCESS);
            socketReceivePoint.setDescription("Reçu : " + format(event.getRawMessage()));
            userFoundPoint.showAnimation();
        });

        EventHandler.registerListener(UserEvent.class, EVENT_TAG, event -> {
            userFoundPoint.setState(LoadState.SUCCESS);
            sendResultPoint.showAnimation();
            sendResultPoint.setState(LoadState.SUCCESS);
        });
        ServerApplication.getServer().startProcess(new DefaultProcessHandler(true, slow));
    }

    private String format(String base) {
        int maxChar = 46;
        int i = Math.min(base.length() - 1, maxChar);
        return base.substring(0, i) + (i < maxChar ? "" : "...");
    }

    public void stopAnimation() {
        if (fingerAnimation != null) {
            fingerAnimation.stop();
        }
    }

    public void startAnimation() {
        finger.setVisible(true);
        legend.setVisible(true);
        this.stopAnimation();
        fingerAnimation = new Timeline();
        fingerAnimation.getKeyFrames().add(new KeyFrame(Duration.seconds(2), new KeyValue(finger.yProperty(), 40)));
        fingerAnimation.setCycleCount(TranslateTransition.INDEFINITE);
        fingerAnimation.setAutoReverse(true);
        fingerAnimation.play();
    }

    private void startResultAnimation(boolean result) {
        finger.setVisible(false);
        legend.setVisible(false);

        Group group = new Group();
        group.setOpacity(0);
        group.setLayoutY(-20);

        Circle circle = new Circle(85, Color.WHITE);
        circle.setStroke(Color.BLACK);
        circle.setStrokeWidth(1);
        circle.setStrokeType(StrokeType.INSIDE);
        circle.setCenterX(ServerApplication.WIDTH/2 + 225 - 42.5 + this.widthOffSet);
        circle.setCenterY(ServerApplication.HEIGHT*3/5 + 60);
        circle.setStyle("-fx-effect: dropshadow(two-pass-box, black, 10, .3, .3, .3);");
        Rectangle rec = new Rectangle(ServerApplication.WIDTH/2 - 225 - 42.5 + this.widthOffSet, ServerApplication.HEIGHT*3/5, 450, 120);
        rec.setArcHeight(20);
        rec.setArcWidth(20);
        rec.setFill(Color.WHITE);
        rec.setStroke(Color.BLACK);
        rec.setStrokeType(StrokeType.INSIDE);
        rec.setStrokeWidth(1);
        rec.setStyle("-fx-effect: dropshadow(two-pass-box, black, 10, .3, .3, .3);");
        Label text = new Label(result ? "VALIDE" : "REFUSE");
        text.setPrefSize(400, 100);
        text.setLayoutX(ServerApplication.WIDTH/2 - 200 - 42.5 - circle.getRadius()*0.3 + this.widthOffSet);
        text.setLayoutY(ServerApplication.HEIGHT*3/5 + 10);
        text.setAlignment(Pos.CENTER);
        text.setTextFill(result ? Color.LIMEGREEN : Color.RED);
        text.setStyle("-fx-font: 80px \"Bauhaus 93\"; -fx-hgap: 15px");
        group.getChildren().add(rec);
        group.getChildren().add(circle);
        group.getChildren().add(text);
        LoadAnimation timeline;
        if (result)
            timeline = this.startCheckAnimation(circle.getCenterX(), circle.getCenterY(), group);
        else
            timeline = this.startCrossAnimation(circle.getCenterX(), circle.getCenterY(), group);

        Timeline fadein = new Timeline();
        fadein.getKeyFrames().addAll(
                new KeyFrame(new Duration(500), (action) -> timeline.play(),
                        new KeyValue(group.opacityProperty(), 1),
                        new KeyValue(group.translateYProperty(), 0))
        );
        pane.getChildren().add(group);
        fadein.play();
    }

    public void showResult(boolean result) {
        stopAnimation();
        startResultAnimation(result);
        finger.setVisible(false);
        legend.setVisible(false);
    }

    private LoadAnimation startCheckAnimation(double x, double y, Group group) {
        LoadAnimation animation = new LoadAnimation(LoadState.SUCCESS, 60, x - 46, y - 30);
        group.getChildren().add(animation);
        return animation;
    }

    private LoadAnimation startCrossAnimation(double x, double y, Group group) {
        LoadAnimation animation = new LoadAnimation(LoadState.ERROR, 60, x - 46, y - 46);
        group.getChildren().add(animation);
        return animation;
    }

    public void restart() {

    }
}
