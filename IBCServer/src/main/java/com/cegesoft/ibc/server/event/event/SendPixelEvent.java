package com.cegesoft.ibc.server.event.event;

import com.cegesoft.ibc.server.event.Event;
import com.cegesoft.ibc.server.utils.image.Location;
import com.cegesoft.ibc.server.utils.image.Pixel;

public class SendPixelEvent extends Event {

    private final Location pixelLocation;
    private final Pixel pixel;

    public SendPixelEvent(Location pixelLocation, Pixel pixel) {
        this.pixelLocation = pixelLocation;
        this.pixel = pixel;
    }

    public Location getPixelLocation() {
        return pixelLocation;
    }

    public Pixel getPixel() {
        return pixel;
    }
}
