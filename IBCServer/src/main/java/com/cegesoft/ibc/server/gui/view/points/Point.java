package com.cegesoft.ibc.server.gui.view.points;

import com.cegesoft.ibc.server.event.Event;
import com.cegesoft.ibc.server.event.EventTask;
import com.cegesoft.ibc.server.gui.view.load.LoadAnimation;
import com.cegesoft.ibc.server.gui.view.load.LoadState;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;

import java.awt.*;

public class Point extends Pane {
    private final int TEXT_MARGIN = 80;

    private LoadState state;
    private String text;
    private final double x;
    private double y;
    private boolean description;

    private Label descriptionLabel;
    private Label label;
    private LoadAnimation animation;

    public Point(LoadState state, String text, double x, double y, boolean description) {
        this.state = state;
        this.text = text;
        this.x = x;
        this.y = y;
        this.description = description;
        this.init();
    }

    private void init() {
        this.setLayoutX(x);
        this.setLayoutY(y);
        this.setPrefWidth(570);
        this.setPrefHeight(45 + (text.contains("\n") ? 25 * (float)text.split("\n").length / 2 : 0) + (description ? 25 : 0));
        this.label = new Label(text);
        this.label.setLayoutX(TEXT_MARGIN);
        this.label.setLayoutY(30);
        this.label.setPrefWidth(this.getPrefWidth() - TEXT_MARGIN);
        this.label.setStyle("-fx-font: 30px 'Bauhaus 93'; -fx-text-fill: WHITE; -fx-effect: dropshadow(two-pass-box, black, 10, .3, .3, .3);");
        this.label.setLineSpacing(-5);
        this.animation = new LoadAnimation(state, 25, 20, 25);
        animation.setVisible(false);
        this.getChildren().add(label);
        this.getChildren().add(animation);
        animation.play();
    }

    public void showAnimation() {
        this.animation.setVisible(true);
    }

    public void hideAnimation() {
        this.animation.setVisible(false);
    }

    public void setState(LoadState state) {
        this.setState(state, true);
    }

    public void setState(LoadState state, boolean restart) {
        Platform.runLater(() -> {
            this.getChildren().remove(this.label);
            this.animation.stop();
            this.getChildren().remove(this.animation);
            this.state = state;
            this.init();
            if (restart)
                showAnimation();
        });
    }

    public void setDescription(String description) {
        if (!this.description) {
            return;
        }
        Platform.runLater(() -> {
            if (this.descriptionLabel != null)
                this.getChildren().remove(this.descriptionLabel);
            this.descriptionLabel = new Label(description);
            this.descriptionLabel.setLayoutY(70 + (text.contains("\n") ? 25 * (float)text.split("\n").length / 2 : 0));
            this.descriptionLabel.setLayoutX(80);
            this.descriptionLabel.setPrefWidth(this.getPrefWidth() - TEXT_MARGIN);
            this.descriptionLabel.setStyle("-fx-font: 20px 'Bauhaus 93'; -fx-text-fill: #242424; -fx-text-alignment: CENTER");
            this.getChildren().add(this.descriptionLabel);
        });
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setText(String text) {
        this.text = text;
        Platform.runLater(() -> {
            this.getChildren().remove(this.label);
            this.animation.stop();
            this.getChildren().remove(this.animation);
            this.init();
            showAnimation();
        });
    }
}
