package com.cegesoft.ibc.server.user;

import com.cegesoft.ibc.server.exceptions.IncorrectPacketFormatException;
import com.cegesoft.ibc.server.packets.IBCServerPacket;

public class User {

    private String id;
    private final Type type;
    private String name;
    private int tickets;
    private int money;
    private boolean result;
    private IBCServerPacket packet;

    public User(String id, String name, Type type){
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public User(String name, int tickets, int money, boolean result, Type type, IBCServerPacket packet) {
        this.name = name;
        this.type = type;
        this.tickets = tickets;
        this.money = money;
        this.result = result;
        this.packet = packet;
    }

    public static User parse(String message, IBCServerPacket packet) throws IncorrectPacketFormatException {
        // NOM@money@ticket@result

        if (!message.contains("@"))
            throw new IncorrectPacketFormatException(message);

        String[] split = message.split("@");

        if (split.length != 4)
            throw new IncorrectPacketFormatException(message);

        String name = split[0];
        int money = Integer.parseInt(split[1]);
        int tickets = Integer.parseInt(split[2]);
        boolean result = Boolean.parseBoolean(split[3]);
        return new User(name, tickets, money, result, Type.CLIENT, packet);
    }

    public void addTickets(int tickets) {
        this.tickets += tickets;
    }

    public int getTickets() {
        return this.tickets;
    }

    public void setTickets(int tickets) {
        this.tickets = tickets;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Type getType() {
        return type;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public String getId() {
        return id;
    }

    public boolean getResult() {
        return result;
    }

    public IBCServerPacket getFirstPacket() {
        return packet;
    }

    public enum Type {
        TERMINAL,
        SERVER,
        CLIENT;

        @Override
        public String toString() {
            return name();
        }
    }
}
