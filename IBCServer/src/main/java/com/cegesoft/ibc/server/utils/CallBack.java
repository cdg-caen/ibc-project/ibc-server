package com.cegesoft.ibc.server.utils;

public interface CallBack<T> {
    void done(T t);
}
