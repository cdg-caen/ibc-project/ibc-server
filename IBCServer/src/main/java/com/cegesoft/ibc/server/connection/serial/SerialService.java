package com.cegesoft.ibc.server.connection.serial;

import com.cegesoft.ibc.server.ServerApplication;
import com.cegesoft.ibc.server.config.ConfigManager;
import com.cegesoft.ibc.server.service.IService;
import com.cegesoft.ibc.server.service.ServiceIdentifier;
import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static com.cegesoft.ibc.server.logging.IBCLogger.getLogger;

public class SerialService extends IService {

    static final int TIMEOUT = 2000;
    private final LinkedHashMap<String, SerialConnection> connections = new LinkedHashMap<>();

    public SerialService(ServiceIdentifier identifier, ServerApplication instance) {
        super(identifier, instance);
    }

    @Override
    public void start() {
        getLogger().info(" ---- Starting Serial ----");
        connections.put(instance.getConfig().getPortName(), new SerialConnection(instance.getConfig().getPortName(), instance.getConfig().getBaud()));
        connections.put(instance.getConfig().getArdPortName(), new SerialConnection(instance.getConfig().getArdPortName(), instance.getConfig().getArdBaud()));
    }

    @Override
    public void stop() {
        for (SerialConnection connection : connections.values()) {
            connection.disconnect();
        }
    }

    public Optional<SerialConnection> getConnection(String portName) {
        SerialConnection connection = this.connections.get(portName);
        return connection == null || connection.isConnected() ? Optional.ofNullable(connection) : Optional.empty();
    }

    public SerialConnection getDefault() {
        return this.connections.get(new ArrayList<>(connections.keySet()).get(0));
    }
}
