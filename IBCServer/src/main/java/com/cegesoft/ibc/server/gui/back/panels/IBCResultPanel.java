package com.cegesoft.ibc.server.gui.back.panels;

import com.cegesoft.ibc.server.gui.back.IBCPanel;
import com.cegesoft.ibc.server.gui.back.IIBCPanel;
import com.cegesoft.ibc.server.gui.PanelState;
import com.cegesoft.ibc.server.tts.TTSManager;
import com.cegesoft.ibc.server.user.User;
import com.voicerss.tts.Languages;

import java.awt.*;
import java.awt.event.KeyEvent;

import static com.cegesoft.ibc.server.logging.IBCLogger.getLogger;

public class IBCResultPanel implements IIBCPanel {

    private boolean loaded;

    public void sendResults(boolean authorized, User user){
        getLogger().info("Bonjour " + user.getName() + ". Il vous reste " + user.getTickets() + " tickets. Vous " + (authorized ? "êtes autorisés à passer !" : "n'êtes pas autorisés à passer !"));
        //TTSManager.create("Bonjour " + user.getName() + ". Il vous reste " + user.getTickets() + " tickets. Vous " + (authorized ? "êtes autorisés à passer !" : "n'êtes pas autorisés à passer !"), Languages.French_France).speech();
    }

    @Override
    public void paintComponents(Graphics g, IBCPanel panel) {

    }

    @Override
    public void onKeyPressed(KeyEvent e, IBCPanel panel) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER){
            panel.setCurrentState(PanelState.CHOICE);
        }
    }

    @Override
    public void onLoad(IBCPanel panel) {
        loaded = true;
        panel.startAnimation("Anim3", false, () -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ignored) { }
            panel.setCurrentState(PanelState.CHOICE);
        });
    }

    @Override
    public void unload(IBCPanel panel) {
        loaded = false;
    }

    @Override
    public boolean isLoad() {
        return loaded;
    }

    @Override
    public Component[] getComponents() {
        return new Component[]{};
    }
}
