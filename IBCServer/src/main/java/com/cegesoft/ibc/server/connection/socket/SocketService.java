package com.cegesoft.ibc.server.connection.socket;

import com.cegesoft.ibc.server.ServerApplication;
import com.cegesoft.ibc.server.message.Message;
import com.cegesoft.ibc.server.service.IService;
import com.cegesoft.ibc.server.service.ServiceIdentifier;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import static com.cegesoft.ibc.server.logging.IBCLogger.getLogger;

public class SocketService extends IService {

    private String ip;
    private List<SocketConnection> connections = new ArrayList<>();

    public SocketService(ServiceIdentifier identifier, ServerApplication server) {
        super(identifier, server);
    }

    private SocketConnection connect() {
        Socket socket = new Socket();
        getLogger().info(" --- Connection to " + ip);
        try {
            socket.connect(new InetSocketAddress(ServerApplication.getServer().getConfig().getIP(), 35000));
        } catch (IOException e) {
            getLogger().error("An error occurred while trying to connect to Socket server.", e);
            return null;
        }
        SocketConnection connection = new SocketConnection(socket);
        connections.add(connection);
        getLogger().info("Connected.");
        return connection;
    }

    @Override
    public void stop() throws IOException {
        for (SocketConnection connection : connections)
            connection.close();
    }

    @Override
    protected void start() {
        this.ip = ServerApplication.getServer().getConfig().getIP();
    }

    public SocketConnection startConnection() {
        return this.connect();
    }

}
