package com.cegesoft.ibc.server.packets;

import com.cegesoft.ibc.server.ServerApplication;

public class IBCServerPacket {

    private final IBCPacketType type;
    private final String token;
    private final String address;
    private final int amount;

    public IBCServerPacket(IBCPacketType type, String token, String address, int amount){
        this.type = type;
        this.token = token;
        this.address = address;
        this.amount = amount;
    }

    @Override
    public String toString() {
        if (ServerApplication.getServer().getConfig().isOverwriteMessage())
            return ServerApplication.getServer().getConfig().getOverwriteMessage();
        return this.type.name() + ":" + this.token + ":" + this.address + ":" + amount + "#";
    }

    public IBCPacketType getType() {
        return type;
    }

    public String getToken() {
        return token;
    }

    public String getAddress() {
        return address;
    }

    public int getAmount() {
        return amount;
    }
}
