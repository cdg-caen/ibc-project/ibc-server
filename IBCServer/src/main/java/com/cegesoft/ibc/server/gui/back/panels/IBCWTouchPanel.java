package com.cegesoft.ibc.server.gui.back.panels;

import com.cegesoft.ibc.server.ServerApplication;
import com.cegesoft.ibc.server.gui.back.IBCPanel;
import com.cegesoft.ibc.server.gui.back.IIBCPanel;
import fr.theshark34.swinger.Swinger;

import java.awt.*;
import java.awt.event.KeyEvent;

public class IBCWTouchPanel implements IIBCPanel {

    private boolean loaded;

    private IBCPanel panel;

    private int y = 0;

    @Override
    public void paintComponents(Graphics g, IBCPanel panel) {
        g.drawImage(Swinger.getResource("doigt.png"), panel.getWidth()/2-222, panel.getHeight()/2-100 - y, 444, 496, panel);
    }

    @Override
    public void onKeyPressed(KeyEvent e, IBCPanel panel) {

    }

    @Override
    public void onLoad(IBCPanel panel) {
        loaded = true;
        this.panel = panel;
        this.panel.startAnimation("Anim2", true, null);

        /*JLabel label = new JLabel("En attente de contact");
        Font f = BauhausFont.BAUHAUS;
        f.deriveFont(35f);
        label.setFont(f);
        label.setForeground(Color.decode("#1A4272"));
        label.setBounds(this.panel.getWidth()/2-200, 180, 400, 50);
        label.setVerticalAlignment(SwingConstants.CENTER);
        label.setHorizontalAlignment(SwingConstants.CENTER);*/


        /*new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            panel.add(label);
            panel.revalidate();
            panel.repaint();
            while (loaded){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                label.setText("En attente de contact.");
                panel.revalidate();
                panel.repaint();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                label.setText("En attente de contact..");
                panel.revalidate();
                panel.repaint();

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                label.setText("En attente de contact...");
                panel.revalidate();
                panel.repaint();

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                label.setText("En attente de contact");
                panel.revalidate();
                panel.repaint();
            }
        }).start();*/
    }

    @Override
    public void unload(IBCPanel panel) {
        loaded = false;
    }

    @Override
    public boolean isLoad() {
        return loaded;
    }

    @Override
    public Component[] getComponents() {
        return new Component[] {};
    }
}
