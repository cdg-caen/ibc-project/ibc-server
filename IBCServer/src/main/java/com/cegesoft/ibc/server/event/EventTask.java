package com.cegesoft.ibc.server.event;

public interface EventTask<T extends Event> {
    void run(T event);
}
