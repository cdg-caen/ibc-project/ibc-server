package com.cegesoft.ibc.server.config;

import com.cegesoft.ibc.server.logging.IBCLogger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;

import static com.cegesoft.ibc.server.logging.IBCLogger.getLogger;

public class ConfigManager {

    private final File file;
    private JSONObject json;

    public ConfigManager() {
        getLogger().info("--- Loading 'terminal.json' ---");
        this.file = new File("terminal.json");
        try {
            if (!file.exists()) {
                getLogger().warn("    File doesn't exist. Generating in process...");
                if (file.createNewFile()) {
                    JSONObject object = new JSONObject();
                    int i;
                    object.put("port", "");
                    object.put("terminal_id", Integer.toString(i = new Random().nextInt(200)));
                    object.put("terminal_name", "TERMINAL-" + i);
                    object.put("debug_level", false);
                    object.put("warn_level", true);
                    object.put("error_level", true);
                    object.put("baud_rate", "9600");
                    json = object;
                    saveConfig();
                    getLogger().info("    File successfully generated");

                }
            }
            IBCLogger.getLogger().info("    Reading file...");

            json = (JSONObject) new JSONParser().parse(new FileReader(file));
            IBCLogger.getLogger().info("    File successfully read !");
            IBCLogger.getLogger().info("----------------------------");
            IBCLogger.getLogger().info(" ");
        } catch (Exception ex) {
            IBCLogger.getLogger().error("An error occurred while creating the terminal config", ex);
        }
        setLoggerLevels();
    }

    private void setLoggerLevels() {
        IBCLogger.DEBUG = (Boolean) json.getOrDefault("debug_level", false);
        IBCLogger.WARN = (Boolean) json.getOrDefault("warn_level", true);
        IBCLogger.ERROR = (Boolean) json.getOrDefault("error_level", true);
    }

    private void saveConfig() throws IOException {
        IBCLogger.getLogger().info("Saving into terminal.json...");
        FileWriter writer = new FileWriter(file);
        writer.write(json.toString());
        writer.flush();
        writer.close();
        IBCLogger.getLogger().info("File saved.");
    }

    public String getPortName() {
        return (String) json.get("port");
    }

    public String getArdTimer() {
        return (String) json.getOrDefault("arduino_timer", "");
    }

    public String getArdPortName() {
        return (String) json.get("arduino_port");
    }

    public String getIP() {
        return (String) json.getOrDefault("ip_socket", "localhost");
    }

    public int getBaud() {
        return Integer.parseInt((String) json.getOrDefault("baud_rate", "9600"));
    }

    public int getArdBaud() {
        return Integer.parseInt((String) json.getOrDefault("arduino_baud_rate", "9600"));
    }

    public String getTerminalID() {
        return (String) json.get("terminal_id");
    }

    public String getTerminalName() {
        return (String) json.get("terminal_name");
    }

    public long getStopTime() {
        return Integer.parseInt("" + json.getOrDefault("stop_byte", 0));
    }

    public long getStopTimeMessage() {
        return Integer.parseInt("" + json.getOrDefault("stop_message", 0));
    }

    public boolean isOverwriteMessage() {
        return (Boolean) json.getOrDefault("overwrite_message_enabled", false);
    }

    public String getOverwriteMessage() {
        return (String) json.getOrDefault("overwrite_message", "a");
    }

    public String getImagePath() {
        return (String) json.getOrDefault("img_path", "./cat.jpg");
    }

    public int getSlowBaud() {
        return Integer.parseInt((String) json.getOrDefault("slow_baud_rate", "5"));
    }
}
