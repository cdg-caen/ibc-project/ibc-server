package com.cegesoft.ibc.server.arduino;

import com.cegesoft.ibc.server.connection.serial.SerialConnection;
import com.cegesoft.ibc.server.connection.serial.SerialService;
import com.cegesoft.ibc.server.service.ServiceManager;

import java.util.Optional;

public abstract class ArduinoInterface {

    private final String portName;
    private Thread runThread;
    private Thread listenerThread;
    private boolean started;
    private final Object lock = new Object();

    protected ArduinoInterface(String portName) {
        this.portName = portName;
    }

    public void start() {
        this.started = true;
        runThread = new Thread(this::run, "ARDUINO-RUN-" + portName);
        runThread.start();

        listenerThread = new Thread(() -> {
            SerialService service = ServiceManager.getService(SerialService.class);
            Optional<SerialConnection> connectionOpt = service.getConnection(portName);
            if (!connectionOpt.isPresent()) {
                this.stop();
                return;
            }
            while (!this.getListenerThread().isInterrupted()) {
                String s;
                if (!(s = connectionOpt.get().read(new byte[5])).equals("")) {
                    onReceive(s);
                }
            }
        }, "ARDUINO-LISTENER-" + portName);
        listenerThread.start();
    }

    public void stop() {
        this.started = false;
        if (this.runThread != null)
            this.runThread.interrupt();
        if (this.listenerThread != null)
            this.listenerThread.interrupt();
        synchronized (lock) {
            lock.notifyAll();
        }
    }

    protected abstract void run();

    protected abstract void onReceive(String t);

    public void waitFor() {
        if (!started)
            return;
        synchronized (lock) {
            try {
                lock.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    protected void send(String t) {
        try {
            Optional<SerialConnection> connectionOpt = ServiceManager.getService(SerialService.class).getConnection(portName);
            if (connectionOpt.isPresent()) {
                connectionOpt.get().write(t, false);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private Thread getListenerThread() {
        return listenerThread;
    }

}
