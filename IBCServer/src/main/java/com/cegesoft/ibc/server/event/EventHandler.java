package com.cegesoft.ibc.server.event;

import java.util.HashMap;
import java.util.Map;

public class EventHandler {

    private static HashMap<Class<? extends Event>, HashMap<String, EventTask<? extends Event>>> events = new HashMap<>();

    public static <T extends Event> void registerListener(Class<T> tClass, String tag, EventTask<T> task) {
        if (!events.containsKey(tClass)) {
            events.put(tClass, new HashMap<>());
        }
        events.get(tClass).put(tag, task);
    }

    public static <T extends Event> void callEvent(T t) {
        if (events.containsKey(t.getClass())) {
            for (Map.Entry<String, EventTask<? extends Event>> task : new HashMap<>(events.get(t.getClass())).entrySet()) {
                ((EventTask<T>) task.getValue()).run(t);
            }
        }
    }

    public static void unregisterListeners(String tag) {
        for (HashMap<String, EventTask<? extends Event>> event : events.values()) {
            for (Map.Entry<String, EventTask<? extends Event>> task : new HashMap<>(event).entrySet()) {
                if (task.getKey().equals(tag))
                    event.remove(tag);
            }
        }
    }
}
