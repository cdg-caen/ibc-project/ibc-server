package com.cegesoft.ibc.server.gui.back.font;

import java.awt.*;
import java.io.IOException;

public class BauhausFont {

    public static Font BAUHAUS;

    public BauhausFont() {
        try {
            BAUHAUS = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/bauhaus.ttf").openStream());


            GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
            genv.registerFont(BAUHAUS);
            BAUHAUS = BAUHAUS.deriveFont(35f);
        } catch (FontFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
