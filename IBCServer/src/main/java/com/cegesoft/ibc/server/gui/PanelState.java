package com.cegesoft.ibc.server.gui;

import com.cegesoft.ibc.server.gui.back.IIBCPanel;
import com.cegesoft.ibc.server.gui.back.panels.IBCChoicePanel;
import com.cegesoft.ibc.server.gui.back.panels.IBCResultPanel;
import com.cegesoft.ibc.server.gui.back.panels.IBCWTouchPanel;

public enum PanelState {

    //SERVER :

    CHOICE(1f, new IBCChoicePanel(), 0, 0),
    WAITING_TOUCH(1f, new IBCWTouchPanel(), 0, 0),
    RESULTS(1f, new IBCResultPanel(), 0, 0);

    private float opacity;
    private final IIBCPanel panel;
    private int xOffSet;
    private int yOffSet;

    PanelState(float opacity, IIBCPanel panel, int xOffSet, int yOffSet){
        this.opacity = opacity;
        this.panel = panel;
        this.xOffSet = xOffSet;
        this.yOffSet = yOffSet;
    }

    public float getOpacity() {
        return opacity;
    }

    public void setOpacity(float opacity) {
        this.opacity = opacity;
    }

    public IIBCPanel getPanel() {
        return panel;
    }

    public int getxOffSet() {
        return xOffSet;
    }

    public void setxOffSet(int xOffSet) {
        this.xOffSet = xOffSet;
    }

    public int getyOffSet() {
        return yOffSet;
    }

    public void setyOffSet(int yOffSet) {
        this.yOffSet = yOffSet;
    }
}
