package com.cegesoft.ibc.server.gui.back;

import com.cegesoft.ibc.server.ServerApplication;
import com.cegesoft.ibc.server.gui.PanelState;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static com.cegesoft.ibc.server.logging.IBCLogger.getLogger;
import static fr.theshark34.swinger.Swinger.getResource;

public class IBCPanel extends JPanel {

    private PanelState currentState;
    private Image animationCurrentImage;
    private Thread animationThread;
    private int animationIndex = 0;

    public IBCPanel(PanelState state) {
        getLogger().info("Setting up IBCPanel");
        this.currentState = state;
        this.currentState.getPanel().onLoad(this);
        this.setLayout(null);
        this.setFocusable(true);
        this.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    ServerApplication.getServer().exit();
                } else {
                    currentState.getPanel().onKeyPressed(e, IBCPanel.this);
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        for (Component comp : state.getPanel().getComponents()) {
            this.add(comp);
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (animationCurrentImage != null) {
            g.drawImage(animationCurrentImage, 0, 0, this.getWidth(), this.getHeight(), this);
        } else {
            g.drawImage(getResource("Anim1/0.png"), 0, 0, this.getWidth(), this.getHeight(), this);
            currentState.getPanel().paintComponents(g, this);
        }
    }

    @Override
    public void repaint() {
        super.repaint();
    }

    public PanelState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(PanelState state) {
        if (this.currentState != null)
            this.currentState.getPanel().unload(this);
        this.currentState = state;
        this.currentState.getPanel().onLoad(this);
        this.repaint();
        this.updateUI();
    }

    public void startAnimation(String folder, boolean loop, Runnable onFinish) {
        if (animationThread != null && !animationThread.isInterrupted()) {
            animationThread.interrupt();
        }
        for (Component comp : this.getComponents())
            this.remove(comp);
        animationIndex = 0;
        animationThread = new Thread(() -> {
            Image img = getResource(folder + "/" + animationIndex + ".png");
            while (!animationThread.isInterrupted()) {
                try {
                    IBCPanel.this.animationCurrentImage = img;
                    IBCPanel.this.repaint();
                    try {
                        Thread.sleep(1000 / 60);
                    } catch (InterruptedException e) {
                        break;
                    }
                    animationIndex+=2;
                    img = getResource(folder + "/" + animationIndex + ".png");
                } catch (IllegalArgumentException ignored) {
                    if (loop) {
                        animationIndex = 0;
                        img = getResource(folder + "/" + animationIndex + ".png");
                    } else
                        break;
                }
            }
            onFinish.run();
        });
        animationThread.start();

    }

    public void exit() {
        currentState.getPanel().unload(this);
    }

}
