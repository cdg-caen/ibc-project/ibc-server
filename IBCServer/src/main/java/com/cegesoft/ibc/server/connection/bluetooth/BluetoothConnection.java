package com.cegesoft.ibc.server.connection.bluetooth;

import com.cegesoft.ibc.server.event.EventHandler;
import com.cegesoft.ibc.server.event.event.BluetoothServiceEvent;
import com.cegesoft.ibc.server.exceptions.InvalidStreamException;
import com.cegesoft.ibc.server.utils.CallBack;

import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;

import static com.cegesoft.ibc.server.logging.IBCLogger.getLogger;

public class BluetoothConnection implements Closeable {

    private final int id;
    private boolean isConnected;
    private StreamConnection client;

    private DataOutputStream outputStream;
    private DataInputStream inputStream;

    private ConnectionThread connectionAttempt;
    private final StreamConnectionNotifier server;
    private final CallBack<ConnectionResult> result;

    BluetoothConnection(StreamConnectionNotifier server, CallBack<ConnectionResult> result) {
        this.id = new Random().nextInt(100);
        this.server = server;
        this.result = result;
        this.connect();
    }

    private void connect(StreamConnectionNotifier server) throws IOException {
        getLogger().info("Waiting Bluetooth connection...");
        client = server.acceptAndOpen();
        getLogger().info(" ========= Client connected =========");
        isConnected = true;
    }

    public void connect() {
        if (isConnected)
            return;
        connectionAttempt = new ConnectionThread(new ConnectionRunnable<IOException>() {
            @Override
            void exec() throws IOException {
                BluetoothConnection.this.connect(server);
            }
        }, "BT-Connection-Thread-" + id, result);
    }

    private void openInputStream() throws InvalidStreamException {
        try {
            this.inputStream = this.client.openDataInputStream();
        } catch (IOException e) {
            throw new InvalidStreamException("Can't open inputStream : ");
        }
    }

    private void openOutputStream() throws InvalidStreamException {
        try {
            this.outputStream = this.client.openDataOutputStream();
        } catch (IOException e) {
            throw new InvalidStreamException("Can't open outputStream : ");
        }
    }

    public void disconnect() throws IOException {
        if (isConnected) {
            getLogger().info("Disconnecting Client...");
            isConnected = false;
            this.client.close();
            getLogger().info("Closing streams...");
            if (this.outputStream != null)
                this.outputStream.close();
            if (this.inputStream != null)
                this.inputStream.close();
            this.client = null;
            this.outputStream = null;
            this.inputStream = null;
            getLogger().info("Client successfully disconnected");
        }
        if (connectionAttempt != null && !connectionAttempt.isInterrupted()) {
            connectionAttempt.interrupt();
            connectionAttempt = null;
        }
    }

    private void writeUTF(String s) throws IOException, InvalidStreamException {
        if (this.outputStream == null)
            this.openOutputStream();
        this.outputStream.writeUTF(s);
        getLogger().info("Wrote in bluetooth connection : " + s);
    }

    private String readUTF() throws IOException, InvalidStreamException {
        if (this.inputStream == null)
            this.openInputStream();
        return this.inputStream.readUTF();
    }

    public void write(String s) {
        try {
            this.writeUTF(s);
        } catch (IOException e) {
            getLogger().error("Can't write packet '" + s + "'.", e);
        } catch (InvalidStreamException e) {
            e.printStackTrace();
        }
    }

    public String read() {
        try {
            return this.readUTF();
        } catch (IOException e) {
            getLogger().error("Can't read packet.", e);
        } catch (InvalidStreamException e) {
            e.printStackTrace();
        }
        return "";
    }

    public byte[] readBytes() {
        try {
            if (this.inputStream == null)
                this.openInputStream();
            byte[] b = new byte[inputStream.available()];
            this.inputStream.readFully(b);
            return b;
        } catch (IOException e) {
            getLogger().error("Can't read packet.", e);
        } catch (InvalidStreamException e) {
            e.printStackTrace();
        } catch (Exception e) {
            getLogger().error("Can't decrypt packet", e);
        }
        return new byte[0];
    }

    public boolean dataAvailable() throws IOException, InvalidStreamException {
        if (!isConnected)
            return false;
        if (this.inputStream == null)
            this.openInputStream();
        return this.inputStream.available() > -1;
    }

    public boolean isConnected() {
        return this.isConnected;
    }

    public int getId() {
        return id;
    }

    @Override
    public void close() throws IOException {
        this.disconnect();
    }

    public class ConnectionThread extends Thread {

        private final CallBack<ConnectionResult> result;

        public ConnectionThread(ConnectionRunnable<IOException> target, String name, CallBack<ConnectionResult> result) {
            super(target, name);
            target.setOnExcept((except) -> result.done(new ConnectionResult(except)));
            this.result = result;
            this.start();
        }

        @Override
        public void run() {
            super.run();
            this.result.done(new ConnectionResult());
        }
    }

    public class ConnectionResult {
        private boolean failed;
        private IOException except;

        public ConnectionResult() {
            this.failed = false;
            this.except = null;
        }

        public ConnectionResult(IOException except) {
            this.failed = true;
            this.except = except;
        }

        public boolean isFailed() {
            return failed;
        }

        public IOException getExcept() {
            return except;
        }
    }

    private abstract class ConnectionRunnable<T extends Exception> implements Runnable {

        private CallBack<T> onExcept;

        abstract void exec() throws T;

        @Override
        public void run() {
            try {
                exec();
            } catch (Exception e) {
                onExcept.done((T) e);
            }
        }

        public ConnectionRunnable setOnExcept(CallBack<T> onExcept) {
            this.onExcept = onExcept;
            return this;
        }
    }
}
