package com.cegesoft.ibc.server.tts;

import javax.sound.sampled.*;
import java.io.IOException;
import java.io.InputStream;

public class TTS {

    private final InputStream in;

    TTS(InputStream in){
        this.in = in;
    }

    public void speech(){
        try {
            AudioInputStream stream = AudioSystem.getAudioInputStream(in);
            AudioFormat format = stream.getFormat();
            int bufferSize = format.getFrameSize() * Math.round(format.getSampleRate() / 10);
            byte[] buffer = new byte[bufferSize];
            SourceDataLine line;
            try {
                DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
                line = (SourceDataLine) AudioSystem.getLine(info);
                line.open(format, bufferSize);
            } catch (LineUnavailableException e) {
                e.printStackTrace();
                return;
            }
            line.start();
            try {
                int numBytesRead = 0;
                while (numBytesRead != -1) {
                    numBytesRead = in.read(buffer, 0, buffer.length);
                    if (numBytesRead != -1)
                        line.write(buffer, 0, numBytesRead);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            line.drain();
            line.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
