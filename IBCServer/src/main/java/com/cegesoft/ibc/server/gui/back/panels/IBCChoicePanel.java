package com.cegesoft.ibc.server.gui.back.panels;

import com.cegesoft.ibc.server.gui.back.IBCPanel;
import com.cegesoft.ibc.server.gui.back.IIBCPanel;
import com.cegesoft.ibc.server.gui.PanelState;
import com.cegesoft.ibc.server.gui.back.font.BauhausFont;
import com.cegesoft.ibc.server.packets.IBCPacketType;
import fr.theshark34.swinger.Swinger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IBCChoicePanel implements IIBCPanel {

    private boolean loaded;

    @Override
    public void paintComponents(Graphics g, IBCPanel obs) {
        int y = 163;
        int x = 304;
        for (IBCPacketType type : IBCPacketType.values()){
            BufferedImage fond = Swinger.getResource(type.equals(IBCPacketType.getSelected())?"select.png":"deselect.png");
            g.drawImage(fond, x + PanelState.CHOICE.getxOffSet(), y + PanelState.CHOICE.getyOffSet(), obs);
            y += fond.getHeight(obs) - 15;
        }
    }

    @Override
    public void onKeyPressed(KeyEvent e, IBCPanel panel) {
        if (e.getKeyCode() == KeyEvent.VK_DOWN){
            int index = Arrays.asList(IBCPacketType.values()).indexOf(IBCPacketType.getSelected());
            if (index + 1 == IBCPacketType.values().length){
                IBCPacketType.setSelected(IBCPacketType.values()[0]);
            } else {
                IBCPacketType.setSelected(IBCPacketType.values()[index+1]);
            }
            panel.repaint();
        } else if (e.getKeyCode() == KeyEvent.VK_UP){
            int index = Arrays.asList(IBCPacketType.values()).indexOf(IBCPacketType.getSelected());
            if (index == 0){
                IBCPacketType.setSelected(IBCPacketType.values()[IBCPacketType.values().length-1]);
            } else {
                IBCPacketType.setSelected(IBCPacketType.values()[index-1]);
            }
            panel.repaint();
        } else if (e.getKeyCode() == KeyEvent.VK_ENTER){
            panel.startAnimation("Anim1", false, () -> panel.setCurrentState(PanelState.WAITING_TOUCH));
        }
    }

    @Override
    public void onLoad(IBCPanel panel) {
        loaded = true;
    }

    @Override
    public void unload(IBCPanel panel) {
        loaded = false;
    }

    @Override
    public boolean isLoad() {
        return loaded;
    }

    @Override
    public Component[] getComponents() {
        List<Component> components = new ArrayList<>();
        int y = 163;
        for (IBCPacketType type : IBCPacketType.values()){
            JLabel label = new JLabel(type.getText());
            label.setForeground(Color.decode("#6D6D6D"));
            label.setFont(BauhausFont.BAUHAUS);
            label.setBounds(304, y, 398, 110);
            label.setHorizontalAlignment(SwingConstants.CENTER);
            label.setVerticalAlignment(SwingConstants.CENTER);
            components.add(label);
            y += 95;
        }
        Component[] comp = new Component[components.size()];
        components.toArray(comp);
        return comp;
    }
}
