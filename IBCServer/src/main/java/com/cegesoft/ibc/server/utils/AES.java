package com.cegesoft.ibc.server.utils;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class AES {

    private static final String STRING_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCO0eHBW3GcH+BXeAy0kBLU09EAZanMAOVe4tQzvAe3jR1IoJTqr/yGugvYwknEJXajW5VF5UulieGlCdetXvCAvDUh85d/Y8n90Eawu9q0dBNxmzSkcR/J41vdaOf4lMrZm1kPiIOepNnTWKFwGgCjurfE9gyl2wz48lhaSAiB46F8ynbDjEirSK2yrzwOyo2R4eoSvZiuG6M0ib146kFqQy1i/ShhaGCOGcxZfttQSch67lYnX2w+THJj2pkWATEJ/6uIfiLQ2B2qEJ/ZKwTdRXn4BbiWeXfilek1T3UBV/JPjm/134ts+b+ct+BnnU0hZnwHmgN4ztz3YtdCGKXbAgMBAAECggEAYxQVcodWrIGsp+Gfps/gUIdVkdn1TpI8jXP81JsYG20pKI5GYP2PyYNd3pUCibGXuUI6r0+gCAo8YLsQRbT8UcK/dXtA2V5H7e3EbK0Vz2RLbN/ISGA2Nl+lBlfVvayzgPz1bwI8lR02BBmTfWp8kRlijaBIK/Nn73hodNFOHZTWvHtm9P0UCmW1UxD6sCIRASjRmCl1C+p3dIMgAvk1MQQ5+qxNCbJGnNhUFctwPoZlwt/woXdMTeJUCGuIVzTUJGyRWtYvRyW4jqPj2d4LNboTMJHNzzKdJ8bcrnw2NTYyT9KdR4N0X2g+hsefCnoO4m2So4EWi6S2QOht0UKt2QKBgQDswZxarix9mXT4yh+f/njx3ZU2l/qqRLiyuT3NQXchHQY0DuF8p0YpVuf8xo2wInnvJw4FAV7VecfQHtoHP8PjImLhXPSBfW99Yz0PFsi8TCTHENRe1tJPEKN+W8wx30nbQw1tU0zDtYVi/UuWgu6w0zWZlHnthBE3Pzagk9+UFQKBgQCabafh6oY6b8m4KJvI2vmQ1IONGct3XX3Vza/+NsdGsMrVfVtoCOZamplwUCLtTxjWkSsLe6sSMoKm/sAYBJ5evTcCpj6ZszFPy4Lvx2ECr34VpSfpUJFq1RD2HKdj/F3P7tSeu/9bkBTH7UzqVYPk3ZdXKwxYH7rTsj7QOGoeLwKBgQDb6/jeLv566SpD1BVOXGZiXW82mDQrJ8zdj779w/ryEnDpuIzyart8uRKjtph07YGQihm2wpAiAo5+H7ptk2jVuuJY7yzcf/UpZx6XvpVWe/CJzXNIaYpQEWedf44NIZoMh5uYWxFqllpCGoG1XQh1zqvybn5YCB6p0BlhThsYBQKBgQCBGZLz5p/4UmQMtMYnkAh6725ak2npjBsali2/FARcMIghGf9EZcxNilkj75qDUVCucAXQP76wI9Hd0bK9YX8ULJLEgD5tN8TNtJDt63xKRnPdnIhVIKmw6haX6TR2/g9fOYY+fWPpI/Z7fQKEFKGCv0057h8PvNbvC2A/qinsuQKBgCb98fA0LPRDMBGv+8hZiO/4KIU/5djs3YcWDLL4JEMimYCWDdsr/6eFIU6pl/mbwPHgD1qt6pDRfZmmZWLgEX9H54skWNdYcTdCXH0ckSh+jH6zhQCbywJe5qUGMT6dp5HT/H4BPeHj8uND/WBWZhmuXDejo1vlSvHw0+FxBFqj";
    private static final String PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkWe6mPVFjgznwF/TnRo+xY0snqfTTqRbnLb2StyXqnoa3jnADVt68SYdfe/tepVp+WWnVjyb5NCaDXjJT0XJBpg8raiThY1APsNuplKEhiZofT0Igk8uTQa+YOM0HwPjcX9tsHwIVdRUGwwJnZLcO/XPbsXHo2NZ2ULTEhalYU+lgihzZK/Wr0j7IyJn0/gUrizsXnmeLJCkWQXBFhuIOZLtnAEb2RvJo2hA5cByPSoj+lpquqhlBVQldmxaFAq+ZLC3zsfSogJ0fu7Qku4K0wepXGlwcAjW+NjHc5SiPEj71WsNo+7htOiH00euc9T52wM40X3xt+6jm+p0iyicJwIDAQAB";

    public static byte[] encrypt(byte[] encrypted) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, getPublicKey());
        return cipher.doFinal(encrypted);
    }

    public static byte[] decrypt(byte[] encrypted) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.DECRYPT_MODE, getPrivateKey(), new IvParameterSpec("AAAAAAAAAAAAAAAA".getBytes(StandardCharsets.UTF_8)));
        return cipher.doFinal(encrypted);
    }

    public static KeyPair generateKey() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(2048);
        return keyGen.generateKeyPair();
    }

    public static PublicKey getPublicKey() {
        try {
            return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.getDecoder().decode(PUBLIC_KEY.getBytes())));
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static PrivateKey getPrivateKey() {
        try {
            return KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(Base64.getDecoder().decode(STRING_KEY.getBytes())));
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }




}
