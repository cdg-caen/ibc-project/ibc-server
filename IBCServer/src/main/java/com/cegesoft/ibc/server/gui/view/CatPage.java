package com.cegesoft.ibc.server.gui.view;

import com.cegesoft.ibc.server.ServerApplication;
import com.cegesoft.ibc.server.event.EventHandler;
import com.cegesoft.ibc.server.event.event.SendPixelEvent;
import com.cegesoft.ibc.server.gui.IGUIPage;
import com.cegesoft.ibc.server.logging.IBCLogger;
import com.cegesoft.ibc.server.process.ImageProcessHandler;
import com.cegesoft.ibc.server.utils.image.ImageDecomposer;
import com.cegesoft.ibc.server.utils.image.Location;
import com.cegesoft.ibc.server.utils.image.Pixel;
import javafx.application.Platform;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.logging.Logger;

import static com.cegesoft.ibc.server.logging.IBCLogger.getLogger;

public class CatPage implements IGUIPage, Initializable {
    public AnchorPane content;
    public Pane pane;
    public Label title;
    public Pane fullImageBackground;
    public Pane currentPixel;
    public Label legend;

    private static boolean needCreate = true;
    private final HashMap<Location, Pane> pixelPanes = new HashMap<>();

    private static final String EVENT_TAG = "CatEventTag";

    public static ImageDecomposer decomposer;

    @Override
    public URL getStageURL() {
        return getClass().getResource("/view/CatPage.fxml");
    }

    @Override
    public void disable() {
        EventHandler.unregisterListeners(EVENT_TAG);
    }

    @Override
    public void enable(String... args) {
        try {
            decomposer = new ImageDecomposer(ImageIO.read(new File(ServerApplication.getServer().getConfig().getImagePath().replaceAll("\\./", System.getProperty("user.dir")))));
            needCreate = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        EventHandler.registerListener(SendPixelEvent.class, EVENT_TAG, this::updateUI);
        ServerApplication.getServer().startProcess(new ImageProcessHandler());
    }

    @Override
    public void resetGUI() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }


    public static void setDecomposer(ImageDecomposer decomposer) {
        CatPage.decomposer = decomposer;
        CatPage.needCreate = true;
    }

    private void updateUI(SendPixelEvent event) {
        Platform.runLater(() -> {
            if (needCreate) {
                for (Pane pane : pixelPanes.values())
                    this.fullImageBackground.getChildren().remove(pane);
                this.pixelPanes.clear();

                int deltaW = decomposer.getWidth() >= decomposer.getHeight() ? 0 : (decomposer.getHeight() - decomposer.getWidth())/2;
                int deltaH = decomposer.getHeight() >= decomposer.getWidth() ? 0 : (decomposer.getWidth() - decomposer.getHeight())/2;

                double width = (this.fullImageBackground.getWidth() / ((double)decomposer.getWidth() + deltaW*2));
                double height = (this.fullImageBackground.getHeight() / ((double)decomposer.getHeight() + deltaH*2));

                for (int i = 0; i < decomposer.getWidth(); i++) {
                    for (int l = 0; l < decomposer.getHeight(); l++) {
                        Pane pane = new Pane();
                        pane.setPrefWidth(width);
                        pane.setPrefHeight(height);
                        pane.setLayoutX(i*width + deltaW*width);
                        pane.setLayoutY(l*height + deltaH*height);
                        pane.setBorder(null);
                        this.fullImageBackground.getChildren().add(pane);
                        pixelPanes.put(new Location(i, l), pane);
                    }
                }
                needCreate = false;
            }
            for (Map.Entry<Location, Pane> pane : pixelPanes.entrySet()) {
                Pixel pixel = decomposer.get(pane.getKey());
                if (pixel == null) {
                    continue;
                }
                pane.getValue().setStyle(String.format("-fx-background-color: rgba(%s, %s, %s, %s);", pixel.getRed(), pixel.getGreen(), pixel.getBlue(), pixel.getAlpha()));
                pane.getValue().setOpacity(event.getPixelLocation().equals(pane.getKey()) ? 1 : 0.7);
                pane.getValue().applyCss();
            }
            currentPixel.setStyle(String.format("-fx-background-color: rgba(%s, %s, %s, %s); -fx-vgap: 10px; -fx-effect: dropshadow(two-pass-box, black, 10, .3, .3, .3);", event.getPixel().getRed(), event.getPixel().getGreen(), event.getPixel().getBlue(), event.getPixel().getAlpha()));
        });
    }
}
