package com.cegesoft.ibc.server.process;

import com.cegesoft.ibc.server.ServerApplication;
import com.cegesoft.ibc.server.connection.serial.SerialConnection;
import com.cegesoft.ibc.server.connection.serial.SerialService;
import com.cegesoft.ibc.server.event.EventHandler;
import com.cegesoft.ibc.server.event.event.SendPixelEvent;
import com.cegesoft.ibc.server.event.event.StateEvent;
import com.cegesoft.ibc.server.gui.view.CatPage;
import com.cegesoft.ibc.server.service.ServiceManager;
import com.cegesoft.ibc.server.utils.image.ImageDecomposer;
import com.cegesoft.ibc.server.utils.image.PixelPacket;

import javax.imageio.ImageIO;

import java.io.File;
import java.io.IOException;

import static com.cegesoft.ibc.server.logging.IBCLogger.getLogger;

public class ImageProcessHandler extends ProcessHandler {

    private boolean started;

    @Override
    protected void execute() {
        this.started = true;
        try {
            EventHandler.callEvent(new StateEvent.Start());
            getLogger().info(" ======== Starting process ========");
            ImageDecomposer decomposer = CatPage.decomposer;
            SerialService serialService = ServiceManager.getService(SerialService.class);
            SerialConnection connection = serialService.getDefault();
            connection.setBaudRate(ServerApplication.getServer().getConfig().getBaud());

            while (started) {
                PixelPacket packet = decomposer.getAndNext();
                EventHandler.callEvent(new SendPixelEvent(packet.getLocation(), packet.getPixel()));
                connection.write(packet.toString(), true);
            }
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void interruptProcess() {
        this.started = false;
    }
}
