package com.cegesoft.ibc.server.event.event;

import com.cegesoft.ibc.server.event.Event;
import com.cegesoft.ibc.server.message.Message;

public class SocketServiceEvent {
    public static class Connect extends Event {

    }

    public static class Send {
        public static class Pre extends Event {

        }
        public static class Post extends Event {
            private final Message message;

            public Post(Message message) {
                this.message = message;
            }

            public Message getMessage() {
                return message;
            }
        }
        public static class Fail extends Event {

        }
    }
    public static class Receive extends Event {
        private final String rawMessage;
        private final Message message;

        public Receive(String rawMessage, Message message) {
            this.message = message;
            this.rawMessage = rawMessage;
        }

        public Message getMessage() {
            return message;
        }

        public String getRawMessage() {
            return rawMessage;
        }
    }
}
