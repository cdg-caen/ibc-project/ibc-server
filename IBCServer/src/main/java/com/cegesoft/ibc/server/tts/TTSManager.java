package com.cegesoft.ibc.server.tts;

import com.voicerss.tts.*;

import java.io.*;

public class TTSManager {

    public static TTS create(Text text) {
        try {
            InputStream in = new ByteArrayInputStream(load(text));
            return new TTS(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new TTS(null);
    }

    private static TTS create(String text, String lang){
        try {
            InputStream in = new ByteArrayInputStream(getBytesOf(text, lang));
            return new TTS(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new TTS(null);
    }

    private static byte[] getBytesOf(String text, String lang) {
        try {
            VoiceProvider tts = new VoiceProvider("ba80c32a1df846c2bc0bcce6d2cde881");
            VoiceParameters params = new VoiceParameters(text, lang);
            params.setCodec(AudioCodec.WAV);
            params.setFormat(AudioFormat.Format_12KHZ.AF_12khz_16bit_stereo);
            params.setBase64(false);
            params.setSSML(false);
            params.setRate(0);

            return tts.speech(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    public enum Text {
        OPEN,
        NOT_AUTHORIZED,
        CLOSING
    }

    private static void save(Text text, byte[] bytes) throws IOException {
        File file = new File(text.name().toLowerCase() + ".ibc");
        if (!file.exists()) {
            if (!file.createNewFile())
                return;
        }
        DataOutputStream out = new DataOutputStream(new FileOutputStream(file));
        out.write(bytes);
        out.flush();
        out.close();
    }

    private static byte[] load(Text text) throws IOException {
        File file = new File(text.name().toLowerCase() + ".ibc");
        if (!file.exists())
            return new byte[0];
        DataInputStream in = new DataInputStream(new FileInputStream(file));
        byte[] b = new byte[in.available()];
        in.readFully(b);
        return b;
    }

    public static void main(String[] args) throws IOException {
        save(Text.NOT_AUTHORIZED, getBytesOf("Vous n'avez plus de titre de transport valide. Accès refusé.", Languages.French_France));
        save(Text.CLOSING, getBytesOf("Fermeture de la porte dans 3 secondes.", Languages.French_France));
        save(Text.OPEN, getBytesOf("Vous êtes autorisé à passer. Ouverture de la porte.", Languages.French_France));
    }

}
