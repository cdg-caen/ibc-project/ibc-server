package com.cegesoft.ibc.server.event.event;

import com.cegesoft.ibc.server.event.Event;
import com.cegesoft.ibc.server.user.User;

public class UserEvent extends Event {
    private final User user;

    public UserEvent(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
