package com.cegesoft.ibc.server.gui.back;


import com.cegesoft.ibc.server.gui.PanelState;

import static com.cegesoft.ibc.server.logging.IBCLogger.getLogger;

import javax.swing.*;

public class IBCFrame extends JFrame {

    public IBCPanel panel;

    public IBCFrame(){
        getLogger().debug("Window settings :");
        this.setSize((int)(1920*0.70), (int)(1024*0.70));
        getLogger().debug(" - Size : W=" + this.getWidth() + ", H=" + this.getHeight());
        this.setUndecorated(true);
        getLogger().debug(" - Setting undecorated");
        this.setContentPane(panel = new IBCPanel(PanelState.CHOICE));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        getLogger().debug(" - Setting default close operation");
        this.setTitle("IBC-SERVER");
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        getLogger().info("---- Displaying the frame ----");
    }

    public void close(){
        getLogger().info("---- Hiding the frame ----");
        this.setVisible(false);
        this.panel.exit();
    }

}
