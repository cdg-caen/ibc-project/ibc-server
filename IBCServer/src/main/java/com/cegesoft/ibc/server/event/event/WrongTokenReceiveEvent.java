package com.cegesoft.ibc.server.event.event;

import com.cegesoft.ibc.server.event.Event;

public class WrongTokenReceiveEvent extends Event {
    private final String token;

    public WrongTokenReceiveEvent(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
