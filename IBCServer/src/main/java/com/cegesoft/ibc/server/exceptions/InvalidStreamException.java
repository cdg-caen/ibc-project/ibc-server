package com.cegesoft.ibc.server.exceptions;

public class InvalidStreamException extends Exception {

    public InvalidStreamException(String msg){
        super(msg);
    }
}
