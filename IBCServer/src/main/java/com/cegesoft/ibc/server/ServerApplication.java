package com.cegesoft.ibc.server;

import com.cegesoft.ibc.server.config.ConfigManager;
import com.cegesoft.ibc.server.connection.bluetooth.BluetoothService;
import com.cegesoft.ibc.server.connection.serial.SerialService;
import com.cegesoft.ibc.server.connection.socket.SocketService;
import com.cegesoft.ibc.server.gpio.GPIOManager;
import com.cegesoft.ibc.server.gui.IGUIPage;
import com.cegesoft.ibc.server.gui.view.MainPage;
import com.cegesoft.ibc.server.gui.view.ProcessPage;
import com.cegesoft.ibc.server.logging.IBCLogger;
import com.cegesoft.ibc.server.process.ProcessHandler;
import com.cegesoft.ibc.server.results.ResultManager;
import com.cegesoft.ibc.server.service.ServiceManager;
import com.cegesoft.ibc.server.user.User;
import fr.theshark34.swinger.Swinger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public final class ServerApplication extends Application {

    public static final double WIDTH = 1600;
    public static final double HEIGHT = 900;
    public static final String VERSION = "1.8.0";
    private static ServerApplication instance;
    private static boolean INIT = false;
    private ConfigManager config;
    private IBCLogger logger;
    private GPIOManager gpio;

    private ProcessHandler processThread;
    private IGUIPage currentController;
    private Stage primaryStage;

    public ServerApplication() {
        instance = this;
    }

    public static ServerApplication getServer() {
        return instance;
    }

    public static void main(String[] args) {
        new ServerApplication().initServer(args);
    }

    public void startProcess(ProcessHandler process) {
        if (processThread != null && !processThread.isInterrupted()) {
            processThread.interrupt();
        }
        this.processThread = process;
    }

    private void initServer(String[] args) {
        if (INIT)
            return;
        INIT = true;
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        this.logger = new IBCLogger();
        this.logger.info("=-=-=-=-=-=- STARTING SERVER (v" + VERSION + ") -=-=-=-=-=-=");
        this.logger.info(" ");
        this.config = new ConfigManager();
        this.logger.info("Registering GPIO Pins");
        this.gpio = new GPIOManager();
        this.gpio.init();
        this.logger.info("Start resources engine");
        Swinger.setSystemLookNFeel();
        Swinger.setResourcePath("/");
        this.logger.info("Registering Services");
        ServiceManager.registerService(BluetoothService.class, this);
        ServiceManager.registerService(SerialService.class, this);
        ServiceManager.registerService(SocketService.class, this);
        ServiceManager.startServices();
        this.logger.info(" ----- Launching Frame ----- ");


        primaryStage = stage;
        primaryStage.setFullScreen(true);
        primaryStage.setFullScreenExitHint("");
        primaryStage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
        primaryStage.setWidth(WIDTH);
        primaryStage.setHeight(HEIGHT);
        primaryStage.setResizable(false);
        primaryStage.setTitle("IBC Server Application");
        primaryStage.setScene(new Scene(new Group()));

        this.navigate(MainPage.class);

        primaryStage.show();

        primaryStage.addEventHandler(KeyEvent.KEY_PRESSED, (event) -> {
            if (event.getCode() == KeyCode.ESCAPE) {
                if (this.currentController instanceof MainPage) {
                    System.exit(0);
                    return;
                }
                this.stopProcess();
                navigate(MainPage.class);
            }
            this.currentController.onKeyPressed(event);
        });
    }

    public void navigate(Class<? extends IGUIPage> page, String... args) {
        try {
            if (currentController != null) {
                currentController.disable();
            }
            FXMLLoader loader = new FXMLLoader(page.newInstance().getStageURL());
            AnchorPane rootLayout = loader.load();
            this.currentController = loader.getController();
            if (!currentController.getStyleSheets().equals(""))
                primaryStage.getScene().getStylesheets().add(currentController.getStyleSheets());
            this.currentController.enable(args);
            primaryStage.getScene().setRoot(rootLayout);
        } catch (Exception ignored) {
        }
    }

    public ConfigManager getConfig() {
        return config;
    }

    public void sendResults(User packet) {
        Platform.runLater(() -> ((ProcessPage) this.currentController).showResult(packet.getResult()));
        ResultManager.sendResults(packet);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void exit() {
        ServiceManager.stopServices();
        System.exit(0);
    }

    public IBCLogger getLogger() {
        return logger;
    }

    public GPIOManager getGPIOManager() {
        return gpio;
    }

    public void stopProcess() {
        if (processThread != null && !processThread.isInterrupted()) {
            processThread.interrupt();
        }
    }

    public IGUIPage getCurrentController() {
        return this.currentController;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }
}
