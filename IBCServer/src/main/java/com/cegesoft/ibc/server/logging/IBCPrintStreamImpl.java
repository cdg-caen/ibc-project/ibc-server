package com.cegesoft.ibc.server.logging;

import java.io.OutputStream;
import java.io.PrintStream;

public class IBCPrintStreamImpl extends PrintStream {

    public IBCPrintStreamImpl(OutputStream stream) {
        super(stream);
    }

    @Override
    public void println(String x) {
        if (x.contains("\n")){
            for (String s : x.split("\n"))
                IBCLogger.getLogger().info(s);
        } else
            IBCLogger.getLogger().info(x);
    }

    public void w(String s){
        super.println(s);
    }
}
