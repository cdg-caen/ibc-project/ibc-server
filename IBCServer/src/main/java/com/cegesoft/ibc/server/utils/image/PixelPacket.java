package com.cegesoft.ibc.server.utils.image;

import javafx.geometry.Dimension2D;

public class PixelPacket {

    private final Pixel pixel;
    private final Location location;
    private final Dimension2D dimension2D;

    public PixelPacket(Pixel pixel, Location location, Dimension2D dimension2D) {
        this.pixel = pixel;
        this.location = location;
        this.dimension2D = dimension2D;
    }

    @Override
    public String toString() {
        return (-((((byte)dimension2D.getHeight() - (byte)dimension2D.getWidth()) << 24) | (((byte) location.getY()) << 16) | (((byte)location.getX()) << 8) | ((byte)dimension2D.getWidth())) + ";" + pixel.toString() + "#").replaceAll(";0;", ";;").replaceAll(";0;", ";;").replaceAll(";0#", ";#");

        //return (((((byte)dimension2D.getWidth()) << 24) | (((byte)dimension2D.getHeight() - (byte)dimension2D.getWidth()) << 16) | (((byte)location.getX()) << 8) | (byte) location.getY()) + ";" + pixel.toString() + "#").replaceAll(";0;", ";;").replaceAll(";0;", ";;").replaceAll(";0#", ";#");
        //return ((int)dimension2D.getWidth() + ";" + ((int)dimension2D.getHeight() - (int)dimension2D.getWidth()) + ";" + location.getX() + ";" + location.getY() + ";" + pixel.toString() + "#").replaceAll(";0;", ";;").replaceAll(";0;", ";;").replaceAll(";0#", ";#");
    }

    public Pixel getPixel() {
        return pixel;
    }

    public Location getLocation() {
        return location;
    }

    public Dimension2D getDimension2D() {
        return dimension2D;
    }
}
