package com.cegesoft.ibc.server.service;

import com.cegesoft.ibc.server.ServerApplication;

public abstract class IService {

    private boolean started;
    private final ServiceIdentifier identifier;
    protected final ServerApplication instance;


    public IService(ServiceIdentifier identifier, ServerApplication instance) {
        this.identifier = identifier;
        this.instance = instance;
    }

    void _start() throws Exception {
        started = true;
        this.start();
    }

    void _stop() throws Exception {
        started = false;
        this.stop();
    }

    protected abstract void start() throws Exception;
    protected abstract void stop() throws Exception;

    protected boolean isStarted(){
        return this.started;
    }

    public ServiceIdentifier getIdentifier() {
        return identifier;
    }
}
