package com.cegesoft.ibc.server.connection.serial;

import com.cegesoft.ibc.server.ServerApplication;
import com.cegesoft.ibc.server.service.ServiceIdentifier;
import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.Scanner;
import java.util.logging.Logger;

import static com.cegesoft.ibc.server.logging.IBCLogger.getLogger;

public class SerialConnection {

    private final String portName;
    private boolean connected;
    private int baudRate;
    private SerialPort port;
    private InputStream in;
    private OutputStream out;
    private boolean baudChanging;
    private boolean isWriting;
    private final Object writingSynchronise = new Object();
    private final Object baudChangingSynchronise = new Object();
    private int newBaudRate = -1;

    SerialConnection(String portName, int baudRate) {
        this.portName = portName;
        this.baudRate = baudRate;
        this.connected = connect();
        if (this.connected) {
            if (initIOStream()) {
                getLogger().info("Serial '" + portName + "' successfully started !");
            }
        }
    }

    private CommPortIdentifier getPortByName(String name) {
        Enumeration ports = CommPortIdentifier.getPortIdentifiers();

        while (ports.hasMoreElements()) {
            CommPortIdentifier curPort = (CommPortIdentifier) ports.nextElement();

            //get only serial ports
            if (curPort.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                if (name.equalsIgnoreCase(curPort.getName())) {
                    return curPort;
                }
            }
        }

        return null;
    }

    private boolean connect() {
        CommPort commPort = null;
        getLogger().info(" => Baud rate : " + baudRate);
        if (portName == null || portName.equals("")) {
            getLogger().warn("Selected Serial Port name is null.");
            getLogger().warn("Please enter a serial port name : ");
            Scanner scanner = new Scanner(System.in);
            String portName = scanner.nextLine();
        }

        getLogger().info(" => Serial port : " + portName);
        CommPortIdentifier comm = getPortByName(portName);;
        if (comm == null) {
            getLogger().error("Serial port '" + portName + "' is not available. Skipping");
            return false;
        }

        int i = 0;

        while (commPort == null) {
            try {
                commPort = comm.open("ApplicationIBC:" + port, SerialService.TIMEOUT);
                ((SerialPort) commPort).setSerialPortParams(
                        baudRate,
                        SerialPort.DATABITS_8,
                        SerialPort.STOPBITS_1,
                        SerialPort.PARITY_NONE);
                getLogger().info("The Serial Communication port has been configured successfully.");
                this.port = (SerialPort) commPort;
                return true;
            } catch (Exception e) {
                i++;
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ignored) {
                }
                getLogger().warn("The Serial Communication port configuration failed. Retry in a second (" + i + " attempts)");
                if (i > 50) {
                    getLogger().error("Can't configure the serial communication port after " + i + " attempts.", e);
                    return false;
                }
            }
        }
        return false;
    }

    private boolean initIOStream() {
        try {
            in = port.getInputStream();
            out = port.getOutputStream();
            getLogger().info("Serial I/O Streams are initialized");
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    void disconnect() {
        try {
            getLogger().info("Disconnecting Serial Communication port...");
            if (this.port != null) {
                this.port.removeEventListener();
                this.port.close();
            }
            if (this.in != null)
                this.in.close();
            if (this.out != null)
                this.out.close();
            getLogger().info("Serial Communication port has been disconnected successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void write(String data, boolean wait) throws InterruptedException {
        synchronized (writingSynchronise) {
            synchronized (baudChangingSynchronise) {
                if (newBaudRate > 0) {
                    newBaudRate = this.setBaudRate(newBaudRate) ? -1 : -2;
                }
                try {
                    isWriting = true;
                    if (baudChanging)
                        baudChangingSynchronise.wait();
                    double ms = ServerApplication.getServer().getConfig().getStopTime() * 1.0;
                    if (ms != 0) {
                        double bit = 1.0 / baudRate;
                        ms = ((wait ? (bit * ms) : 0) + 9.0 * bit) * 1000.0;
                    }
                    for (char c : data.toCharArray()) {
                        if (ms != 0)
                            Thread.sleep((long) ms);
                        out.write((byte) c);
                    }
                } catch (Exception e) {
                    if (e instanceof InterruptedException)
                        throw (InterruptedException) e;
                    else
                        e.printStackTrace();
                } finally {
                    isWriting = false;
                    writingSynchronise.notifyAll();
                }
            }
        }
    }

    public String read(byte[] buff) {
        try {
            DataInputStream stream = new DataInputStream(in);
            stream.readFully(buff);
            return new String(buff, StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public boolean setBaudRateAsync(int i) {
        synchronized (baudChangingSynchronise) {
            newBaudRate = i;
            try {
                baudChangingSynchronise.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return newBaudRate == -1;
    }

    public boolean setBaudRate(int i) {
        synchronized (baudChangingSynchronise) {
            synchronized (writingSynchronise) {
                int back = this.baudRate;
                boolean returnBool = false;
                try {
                    getLogger().info("1");
                    baudChanging = true;
                    if (isWriting) {
                        getLogger().info("2");
                        writingSynchronise.wait();
                    }
                    getLogger().info("3");
                    this.baudRate = i;
                    getLogger().info("Changing Baud rate to " + i);
                    this.port.setSerialPortParams(this.baudRate, SerialPort.DATABITS_8,
                            SerialPort.STOPBITS_1,
                            SerialPort.PARITY_NONE);
                    returnBool = true;
                } catch (Exception e) {
                    getLogger().error("Can't restart Serial : ", e);
                    this.baudRate = back;
                }finally {
                    baudChanging = false;
                    baudChangingSynchronise.notifyAll();
                }
                return returnBool;
            }
        }
    }

    public boolean isConnected() {
        return connected;
    }
}
