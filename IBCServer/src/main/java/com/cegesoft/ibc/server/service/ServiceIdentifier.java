package com.cegesoft.ibc.server.service;

import java.util.UUID;

public class ServiceIdentifier {

    private final UUID identifier;

    ServiceIdentifier(){
        identifier = UUID.randomUUID();
    }

    public UUID getIdentifier() {
        return identifier;
    }
}
