package com.cegesoft.ibc.server.packets;

import com.cegesoft.ibc.server.arduino.DoorArduinoInterface;
import com.cegesoft.ibc.server.user.User;

public enum IBCPacketType {
    M("Carte bleue", 174, 35) {
        @Override
        public void authorize(User user) {
            //TTSManager.create("Vous venez d'être débité de " + user.getFirstPacket().getAmount() + " €", Languages.French_France).speech();
        }

        @Override
        public void refuse(User user) {

        }
    },

    U("Playlist musicale", 245, 33) {
        @Override
        public void authorize(User user) {
            //TTSManager.create("Ok, je lance la musique !", Languages.French_France).speech();
        }

        @Override
        public void refuse(User user) {

        }
    },

    D("Ouverture de porte", 262, 31) {
        @Override
        public void authorize(User user) {
            DoorArduinoInterface doorArduinoInterface = new DoorArduinoInterface(true, user);
            doorArduinoInterface.start();
            doorArduinoInterface.waitFor();
        }

        @Override
        public void refuse(User user) {
            DoorArduinoInterface doorArduinoInterface = new DoorArduinoInterface(false, user);
            doorArduinoInterface.start();
            doorArduinoInterface.waitFor();
        }
    };

    private static IBCPacketType selected = D;
    private final String textPath;
    private final int width;
    private final int height;

    IBCPacketType(String textPath, int width, int height) {
        this.textPath = textPath;
        this.width = width;
        this.height = height;
    }

    public static IBCPacketType getSelected() {
        return selected;
    }

    public static void setSelected(IBCPacketType selected) {
        IBCPacketType.selected = selected;
    }

    public abstract void authorize(User user);

    public abstract void refuse(User user);

    public String getText() {
        return textPath;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
