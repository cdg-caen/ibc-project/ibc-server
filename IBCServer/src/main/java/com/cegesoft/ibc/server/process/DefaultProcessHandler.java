package com.cegesoft.ibc.server.process;

import com.cegesoft.ibc.server.ServerApplication;
import com.cegesoft.ibc.server.connection.bluetooth.BluetoothConnection;
import com.cegesoft.ibc.server.connection.bluetooth.BluetoothService;
import com.cegesoft.ibc.server.connection.serial.SerialConnection;
import com.cegesoft.ibc.server.connection.serial.SerialService;
import com.cegesoft.ibc.server.connection.socket.SocketConnection;
import com.cegesoft.ibc.server.connection.socket.SocketService;
import com.cegesoft.ibc.server.event.EventHandler;
import com.cegesoft.ibc.server.event.event.*;
import com.cegesoft.ibc.server.exceptions.IncorrectPacketFormatException;
import com.cegesoft.ibc.server.gui.view.MainPage;
import com.cegesoft.ibc.server.gui.view.ProcessPage;
import com.cegesoft.ibc.server.message.Message;
import com.cegesoft.ibc.server.packets.IBCPacketType;
import com.cegesoft.ibc.server.packets.IBCServerPacket;
import com.cegesoft.ibc.server.service.ServiceManager;
import com.cegesoft.ibc.server.user.TerminalUser;
import com.cegesoft.ibc.server.user.User;
import com.cegesoft.ibc.server.utils.SoundUtil;

import javax.bluetooth.LocalDevice;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import java.io.IOException;
import java.util.UUID;

import static com.cegesoft.ibc.server.logging.IBCLogger.getLogger;
import static java.nio.charset.StandardCharsets.UTF_8;

public class DefaultProcessHandler extends ProcessHandler {

    private BluetoothConnection bluetooth;
    private SocketConnection socket;
    private final boolean restart;
    private final boolean slow;

    public DefaultProcessHandler(boolean restart, boolean slow) {
        super();
        this.restart = restart;
        this.slow = slow;
    }

    @Override
    protected void execute() {
        try {
            EventHandler.callEvent(new StateEvent.Start());
            getLogger().info(" ======== Starting process ========");
            String token = UUID.randomUUID().toString().split("-")[2];
            getLogger().info("Generated token : " + token);
            getLogger().info(" ");
            EventHandler.callEvent(new TokenCreateEvent(token));
            EventHandler.callEvent(new BluetoothServiceEvent.Start());
            bluetooth = ServiceManager.getService(BluetoothService.class).waitConnection(result -> {
                if (result.isFailed()) {
                    EventHandler.callEvent(new BluetoothServiceEvent.Connect.Fail());
                } else {
                    EventHandler.callEvent(new BluetoothServiceEvent.Connect.Post());
                }
            });

            SerialConnection serial = ServiceManager.getService(SerialService.class).getDefault();

            if (slow)
                serial.setBaudRate(ServerApplication.getServer().getConfig().getSlowBaud());
            else
                serial.setBaudRate(ServerApplication.getServer().getConfig().getBaud());

            try {
                IBCServerPacket serverPacket = new IBCServerPacket(IBCPacketType.getSelected(), token, LocalDevice.getLocalDevice().getBluetoothAddress(), IBCPacketType.getSelected().equals(IBCPacketType.M) ? 20 : 1);
                EventHandler.callEvent(new SerialSendEvent(serverPacket.toString()));
                String msg;
                while (true) {
                    long timeWait = ServerApplication.getServer().getConfig().getStopTimeMessage();
                    while (!bluetooth.isConnected()) {
                        if (timeWait != 0 && !slow)
                            Thread.sleep(timeWait);
                        serial.write(serverPacket.toString(), !slow);
                        if (this.isInterrupted()) {
                            throw new InterruptedException();
                        }
                    }
                    getLogger().info("Bluetooth data are available");

                    Thread.sleep(500);
                    msg = new String(bluetooth.readBytes(), UTF_8);
                    if (msg.contains(";") && msg.split(";")[2].equals(token))
                        break;
                    else {
                        EventHandler.callEvent(new WrongTokenReceiveEvent(msg.split(";")[0]));
                        getLogger().info("Wrong token. Skip");
                        bluetooth.disconnect();
                        bluetooth.connect();
                    }
                }

                SoundUtil.tone(new int[]{523, 783, 1046, 1568, 2093}, new int[]{100, 100, 100, 100, 140}, 100, true);

                try (SocketConnection so = this.socket = ServiceManager.getService(SocketService.class).startConnection()) {
                    EventHandler.callEvent(new BluetoothServiceEvent.Receive(msg));
                    getLogger().info(msg);
                    EventHandler.callEvent(new SocketServiceEvent.Send.Pre());
                    Message send = new Message(Message.Type.GET, Message.Request.QUERY, TerminalUser.toUser(), msg);
                    getLogger().info("Sending to Server...");
                    socket.sendMessage(send);
                    EventHandler.callEvent(new SocketServiceEvent.Send.Post(send));

                    String response = socket.waitForAnswer();
                    Message message = new Message(response);
                    if (message.getRequest().equals(Message.Request.DECLARE)) {
                        getLogger().info("Declaration request received. Send information");
                        try {
                            socket.sendMessage(TerminalUser.declare());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        message = new Message(response = socket.waitForAnswer());
                        getLogger().info("Decrypted data received.");
                    }
                    EventHandler.callEvent(new SocketServiceEvent.Receive(response, message));
                    User user = User.parse(message.getArgs()[0], serverPacket);
                    EventHandler.callEvent(new UserEvent(user));
                    EventHandler.callEvent(new StateEvent.SendResult(user));
                } catch (IncorrectPacketFormatException e) {
                    e.printStackTrace();
                }
            } catch (IOException | LineUnavailableException e) {
                e.printStackTrace();
            }
        } catch (InterruptedException ignored) {
        } finally {
            boolean fail = false;
            try {
                if (this.bluetooth != null)
                    this.bluetooth.close();
                if (this.socket != null)
                    this.socket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
                ServerApplication.getServer().navigate(MainPage.class);
                fail = true;
            }
            if (!fail) {
                if (restart && !interrupted)
                    ServerApplication.getServer().navigate(ProcessPage.class, ((ProcessPage) ServerApplication.getServer().getCurrentController()).demo ? "demo" : "normal", "normal");
                else
                    ServerApplication.getServer().navigate(MainPage.class);
            }
        }
    }

    @Override
    public void interruptProcess() {
        try {
            if (this.bluetooth != null)
                this.bluetooth.close();
            if (this.socket != null)
                this.socket.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
