package com.cegesoft.ibc.server.connection.bluetooth;

import com.cegesoft.ibc.server.ServerApplication;
import com.cegesoft.ibc.server.service.IService;
import com.cegesoft.ibc.server.service.ServiceIdentifier;
import com.cegesoft.ibc.server.utils.CallBack;

import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnectionNotifier;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.cegesoft.ibc.server.logging.IBCLogger.getLogger;


// http://curiosity-addiction.blogspot.com/2013/12/client-server-bluetooth-en-java-le-code.html

public class BluetoothService extends IService {

    private StreamConnectionNotifier server;
    private List<BluetoothConnection> connections = new ArrayList<>();

    public BluetoothService(ServiceIdentifier identifier, ServerApplication server) {
        super(identifier, server);
    }

    public BluetoothConnection waitConnection(CallBack<BluetoothConnection.ConnectionResult> result) {
        BluetoothConnection connection = new BluetoothConnection(server, result);
        this.connections.add(connection);
        return connection;
    }

    @Override
    protected void start() throws IOException {
        getLogger().info(" ---- Starting Bluetooth ----");
        final String myServiceName = "IBCService";

        // 000000000000000000000000500b34fb
        final String myServiceUUID = "000000000000000000000000500b34fb";
        UUID MYSERVICEUUID_UUID = new UUID(myServiceUUID, false);

        // Définit l'url du service.
        //localhost car on est le serveur
        String connURL = "btspp://localhost:" + MYSERVICEUUID_UUID.toString() + ";name=" + myServiceName;

        getLogger().info("Created service :");
        getLogger().info(" - Name : " + myServiceName);
        getLogger().info(" - ServiceUUID : " + myServiceUUID);
        //on se rend découvrable
        LocalDevice.getLocalDevice().setDiscoverable(DiscoveryAgent.GIAC);
        getLogger().info("Setting up discoverable to GIAC");

        // On publie le service record dans le SRDB (Service record database)
        getLogger().info("Publishing service");
        server = (StreamConnectionNotifier)
                Connector.open(connURL);

        getLogger().info("Service published successfully");
    }

    @Override
    protected void stop() throws IOException {
        for (BluetoothConnection connection : connections)
            connection.close();
        server.close();
    }
}
