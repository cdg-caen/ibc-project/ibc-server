package com.cegesoft.ibc.server.gui.view.load;

public enum LoadState {
    LOADING,
    SUCCESS,
    ERROR
}
