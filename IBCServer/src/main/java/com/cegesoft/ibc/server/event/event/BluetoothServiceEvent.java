package com.cegesoft.ibc.server.event.event;

import com.cegesoft.ibc.server.event.Event;

public class BluetoothServiceEvent {
    public static class Start extends Event {

    }

    public static class Connect {
        public static class Pre extends Event {

        }

        public static class Post extends Event {
        }

        public static class Fail extends Event {
        }
    }

    public static class Receive extends Event {
        private final String received;

        public Receive(String received) {
            this.received = received;
        }

        public String getReceived() {
            return received;
        }
    }
}
