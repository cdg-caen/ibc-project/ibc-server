package com.cegesoft.ibc.server.service;

import com.cegesoft.ibc.server.ServerApplication;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import static com.cegesoft.ibc.server.logging.IBCLogger.getLogger;

public class ServiceManager {

    private static final HashMap<Class<? extends IService>, IService> services = new HashMap<>();

    private static final String PREFIX = "[SERVICE MANAGER] ";

    public static void registerService(Class<? extends IService> service, ServerApplication server) {
        if (services.containsKey(service))
            return;
        try {
            getLogger().info(PREFIX + "Register service " + service.getSimpleName());
            services.put(service, service.getDeclaredConstructor(ServiceIdentifier.class, ServerApplication.class).newInstance(new ServiceIdentifier(), server));
            getLogger().info(PREFIX + "Service " + service.getSimpleName() + "registered !");
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            getLogger().error(PREFIX + "Can't register service " + service.getSimpleName() + " : ", e);
        }
    }

    public static void startServices() {
        getLogger().info(PREFIX + "Starting services...");
        for (IService service : services.values())
            startService(service);
    }

    public static boolean startService(Class<? extends IService> service){
        return startService(services.getOrDefault(service, null));
    }

    private static boolean startService(IService service) {
        if (service != null && !service.isStarted()) {
            getLogger().info(PREFIX + "Starting service " + service.getClass().getSimpleName());
            try {
                service._start();
                return true;
            } catch (Exception e) {
                getLogger().error(PREFIX + "Failure : ", e);
            }
        } else if (service != null) {
            getLogger().info(PREFIX + "Service " + service.getClass().getSimpleName() + " is already started. Skip");
            return true;
        }
        return false;
    }

    public static void stopServices() {
        getLogger().info(PREFIX + "Stopping services...");
        for (IService service : services.values())
            stopService(service);
    }

    public static boolean stopService(Class<? extends IService> service){
        return stopService(services.getOrDefault(service, null));
    }

    private static boolean stopService(IService service) {
        if (service != null && service.isStarted()) {
            getLogger().info(PREFIX + "Stopping service " + service.getClass().getSimpleName());
            try {
                service._stop();
                return true;
            } catch (Exception e) {
                getLogger().error(PREFIX + "Failure : ", e);
            }
        } else if (service != null) {
            getLogger().info(PREFIX + "Service " + service.getClass().getSimpleName() + " is already stopped. Skip");
            return true;
        }
        return false;
    }

    public static <T extends IService> T getService(Class<T> tClass) {
        return (T) services.getOrDefault(tClass, null);
    }

}
