package com.cegesoft.ibc.server.event.event;

import com.cegesoft.ibc.server.event.Event;
import com.cegesoft.ibc.server.user.User;

public class StateEvent {
    public static class Start extends Event {

    }

    public static class SendResult extends Event {
        private final User user;

        public SendResult(User user) {
            this.user = user;
        }

        public User getUser() {
            return user;
        }
    }

    public static class Restart extends Event {

    }
}
