package com.cegesoft.ibc.server.gui.view.load;

import javafx.animation.*;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.util.Duration;

public class LoadAnimation extends Pane {
    private final LoadState state;
    private final double x;
    private final double y;
    public Animation line;
    private double length;

    public LoadAnimation(LoadState state, double length, double x, double y) {
        this.state = state;
        this.length = length;
        this.x = x;
        this.y = y;
        this.init();
    }

    private void init() {
        this.setPrefHeight(length);
        this.setPrefWidth(length);
        this.setLayoutY(this.y);
        this.setLayoutX(this.x);
        switch (state) {
            case SUCCESS:
                final double xMax = length * 0.6;
                final double xMaxOffSet = length;
                final double yMaxOffSet = xMax * 2.08;
                Line line1 = new Line();
                line1.setStartY(yMaxOffSet - xMax);
                line1.setStartX(0);
                line1.setEndX(0);
                line1.setEndY(yMaxOffSet - xMax);
                line1.setStrokeWidth(0.5 * xMax);
                line1.setStroke(Color.LIMEGREEN);
                line1.setStrokeLineCap(StrokeLineCap.ROUND);
                line1.setOpacity(0);

                Line line2 = new Line();
                line2.setStartY(yMaxOffSet);
                line2.setStartX(xMax);
                line2.setEndX(xMax);
                line2.setEndY(yMaxOffSet);
                line2.setSmooth(true);
                line2.setStrokeWidth(0.5 * xMax);
                line2.setStroke(Color.LIMEGREEN);
                line2.setStrokeLineCap(StrokeLineCap.ROUND);
                line2.setOpacity(0);

                this.getChildren().add(line1);
                this.getChildren().add(line2);
                Timeline timeline2 = new Timeline();
                timeline2.getKeyFrames().addAll(
                        new KeyFrame(Duration.ONE, new KeyValue(line2.opacityProperty(), 1)),
                        new KeyFrame(new Duration(230),
                                new KeyValue(line2.endXProperty(), xMax + xMaxOffSet),
                                new KeyValue(line2.endYProperty(), 0)
                        )
                );
                Timeline successTimeLine = new Timeline();
                successTimeLine.getKeyFrames().addAll(
                        new KeyFrame(Duration.ONE, new KeyValue(line1.opacityProperty(), 1)),
                        new KeyFrame(new Duration(100), (action) -> timeline2.play(),
                                new KeyValue(line1.endXProperty(), xMax),
                                new KeyValue(line1.endYProperty(), yMaxOffSet)
                        )
                );
                this.line = successTimeLine;
                break;
            case ERROR:
                length *= 1.5;
                Line line3 = new Line();
                line3.setStartY(0);
                line3.setStartX(0);
                line3.setEndX(0);
                line3.setEndY(0);
                line3.setStrokeWidth(0.2 * length);
                line3.setStroke(Color.RED);
                line3.setStrokeLineCap(StrokeLineCap.ROUND);
                line3.setOpacity(0);

                Line line4 = new Line();
                line4.setStartY(0);
                line4.setStartX(length);
                line4.setEndX(length);
                line4.setEndY(0);
                line4.setSmooth(true);
                line4.setStrokeWidth(0.2 * length);
                line4.setStroke(Color.RED);
                line4.setStrokeLineCap(StrokeLineCap.ROUND);
                line4.setOpacity(0);

                this.getChildren().add(line3);
                this.getChildren().add(line4);
                Timeline timeline1 = new Timeline();
                timeline1.getKeyFrames().addAll(
                        new KeyFrame(Duration.ONE, new KeyValue(line4.opacityProperty(), 1)),
                        new KeyFrame(new Duration(230),
                                new KeyValue(line4.endXProperty(), 0),
                                new KeyValue(line4.endYProperty(), length)
                        )
                );
                Timeline errorTimeline = new Timeline();
                errorTimeline.getKeyFrames().addAll(
                        new KeyFrame(Duration.ONE, new KeyValue(line3.opacityProperty(), 1)),
                        new KeyFrame(new Duration(230), (action) -> timeline1.play(),
                                new KeyValue(line3.endXProperty(), length),
                                new KeyValue(line3.endYProperty(), length)
                        )
                );
                this.line = errorTimeline;
                break;
            case LOADING:
                Arc donutArc = new Arc(x, y - 5, length, length, 0, 290);
                donutArc.setStrokeWidth(0.24*length);
                donutArc.setStrokeType(StrokeType.CENTERED);
                donutArc.setStroke(Color.WHITE);
                donutArc.setStrokeLineCap(StrokeLineCap.BUTT);
                donutArc.setFill(null);


                this.getChildren().add(donutArc);
                RotateTransition transition = new RotateTransition(Duration.seconds(2.5), donutArc);
                transition.setCycleCount(TranslateTransition.INDEFINITE);
                transition.setByAngle(0);
                transition.setToAngle(360);
                transition.setInterpolator(Interpolator.LINEAR);
                this.line = transition;
        }
    }

    public void play() {
        line.play();
    }

    public void stop() {
        line.stop();
    }
}
