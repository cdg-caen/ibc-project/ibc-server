package com.cegesoft.ibc.server.message;


import com.cegesoft.ibc.server.exceptions.IncorrectPacketFormatException;
import com.cegesoft.ibc.server.user.User;

public class Message {

    private final Type type;
    private final Request request;
    private final String[] args;
    private User sender;

    public Message(String message) throws IncorrectPacketFormatException {
        if (!message.contains(":") || message.split(":").length < 3) {
            throw new IncorrectPacketFormatException(message);
        }
        String[] split = message.split(":");

        try {
            type = Type.valueOf(split[0].toUpperCase());
        } catch (Exception ex) {
            throw new IncorrectPacketFormatException(message, ex);
        }

        try {
            request = Request.valueOf(split[1].toUpperCase());
        } catch (Exception ex) {
            throw new IncorrectPacketFormatException(message, ex);
        }

        String id = split[2];

        sender = new User(id, "SERVER", User.Type.SERVER);

        args = new String[split.length - 3];

        System.arraycopy(split, 3, args, 0, split.length - 3);
    }

    public Message(Type type, Request request, User user, String... args) {
        this.type = type;
        this.request = request;
        this.sender = user;
        this.args = args;
    }

    @Override
    public String toString() {
        return "Message[type=" + type.name() + ", request=" + request.name() + "]";
    }

    public String parse() {
        StringBuilder args = new StringBuilder();
        if (this.args.length >= 1)
            args.append(this.args[0]);
        if (this.args.length >= 2)
            for (int i = 1; i < this.args.length; i++)
                args.append(":").append(this.args[i]);
        return type.name() + ":" + request.name() + ":" + sender.getId() + ":" + args.toString();
    }

    public Type getType() {
        return type;
    }

    public User getSender() {
        return sender;
    }

    public String[] getArgs() {
        return args;
    }

    public Request getRequest() {
        return request;
    }

    public enum Type {
        GET,
        POST;

        @Override
        public String toString() {
            return name();
        }
    }

    public enum Request {
        MONEY,
        TICKETS,
        CODE,
        DECLARE, QUERY;

        @Override
        public String toString() {
            return name();
        }
    }

}
