package com.cegesoft.ibc.server.gui;

import javafx.scene.input.KeyEvent;

import java.net.URL;

public interface IGUIPage {
    URL getStageURL();
    void disable();
    void enable(String... args);

    default String getStyleSheets(){
        return "";
    }

    void resetGUI();

    default void onKeyPressed(KeyEvent event) {}
}
