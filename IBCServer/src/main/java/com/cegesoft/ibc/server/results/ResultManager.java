package com.cegesoft.ibc.server.results;

import com.cegesoft.ibc.server.packets.IBCPacketType;
import com.cegesoft.ibc.server.tts.TTSManager;
import com.cegesoft.ibc.server.user.User;
import com.voicerss.tts.Languages;

public class ResultManager {
    public static void sendResults(User packet) {
        boolean result = packet.getResult();
        if (result)
            IBCPacketType.getSelected().authorize(packet);
        else
            IBCPacketType.getSelected().refuse(packet);
    }
}
