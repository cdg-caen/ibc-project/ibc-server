package com.cegesoft.ibc.server.user;

import com.cegesoft.ibc.server.ServerApplication;

public class TerminalUser {

    public static String getId() {
        return ServerApplication.getServer().getConfig().getTerminalID();
    }

    public static String getName() {
        return ServerApplication.getServer().getConfig().getTerminalName();
    }

    public static User.Type getType(){
        return User.Type.TERMINAL;
    }

    public static User toUser(){
        return new User(getId(), getName(), getType());
    }

    public static String declare() {
        return TerminalUser.getId() + "#" + TerminalUser.getName() + "#" + TerminalUser.getType();
    }
}
