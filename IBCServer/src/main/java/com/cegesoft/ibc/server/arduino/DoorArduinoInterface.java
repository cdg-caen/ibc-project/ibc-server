package com.cegesoft.ibc.server.arduino;

import com.cegesoft.ibc.server.ServerApplication;
import com.cegesoft.ibc.server.connection.socket.SocketConnection;
import com.cegesoft.ibc.server.connection.socket.SocketService;
import com.cegesoft.ibc.server.message.Message;
import com.cegesoft.ibc.server.service.ServiceManager;
import com.cegesoft.ibc.server.tts.TTSManager;
import com.cegesoft.ibc.server.user.User;
import com.voicerss.tts.Languages;

import java.io.IOException;

import static com.cegesoft.ibc.server.logging.IBCLogger.getLogger;

public class DoorArduinoInterface extends ArduinoInterface {

    private boolean open;
    private final User user;

    public DoorArduinoInterface(boolean open, User user) {
        super(ServerApplication.getServer().getConfig().getArdPortName());
        this.open = open;
        this.user = user;
    }

    @Override
    protected void run() {
        TTSManager.create(open ? TTSManager.Text.OPEN : TTSManager.Text.NOT_AUTHORIZED).speech();
        this.send(open ? "open" : "close");
    }

    @Override
    protected void onReceive(String t) {
        getLogger().info("Reception de " + t);
        if (t.contains("end")) {
            try {
                if (open) {
                    int sec = Integer.parseInt(ServerApplication.getServer().getConfig().getArdTimer());
                    Thread.sleep(1500);
                    TTSManager.create(TTSManager.Text.CLOSING).speech();
                    this.send("close" + sec);
                    this.open = false;
                    return;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.stop();
        }
    }
}
