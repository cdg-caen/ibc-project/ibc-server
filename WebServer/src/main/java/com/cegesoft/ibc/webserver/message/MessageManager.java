package com.cegesoft.ibc.webserver.message;

import com.cegesoft.ibc.webserver.Main;
import com.cegesoft.ibc.webserver.config.ConfigManager;
import com.cegesoft.ibc.webserver.connection.ConnectionManager;
import com.cegesoft.ibc.webserver.exceptions.LowPermissionException;
import com.cegesoft.ibc.webserver.exceptions.MessageBadlyFormattedException;
import com.cegesoft.ibc.webserver.logging.IBCLogger;
import com.cegesoft.ibc.webserver.user.User;
import com.cegesoft.ibc.webserver.utils.AES;
import com.cegesoft.ibc.webserver.utils.CryptoHandler;
import com.cegesoft.ibc.webserver.utils.IBCClientPacket;
import org.apache.commons.codec.binary.Base64;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class MessageManager {

    public static void query(String message, ConnectionManager.Client socket) throws MessageBadlyFormattedException, LowPermissionException {
        Message msg = new Message(message, socket);
        IBCLogger.getLogger().info("Analysis : SUCCESS");
        IBCLogger.getLogger().debug(" ");
        IBCLogger.getLogger().debug("-- Message information --");
        IBCLogger.getLogger().debug("=> TYPE = " + msg.getType());
        IBCLogger.getLogger().debug("=> REQUEST = " + msg.getRequest());
        IBCLogger.getLogger().debug("=> SENDER = " + msg.getSender().getType() + " (" + msg.getSender().getId() + ")");
        IBCLogger.getLogger().debug("=> ARGS_LENGTH = " + msg.getArgs().length);
        IBCLogger.getLogger().debug(" ");
        if (!msg.getSender().hasPermission(msg))
            throw new LowPermissionException(msg.getSender(), msg);

        IBCLogger.getLogger().info("Checking permission : SUCCESS");

        if (msg.getRequest().equals(Message.Request.DECLARE)) {
            IBCLogger.getLogger().info("Declaring user...");
            User user = msg.getDeclaredUser(msg.getArgs()[0]);
            if (user == null) {
                IBCLogger.getLogger().info("- FAILED");
                try {
                    IBCLogger.getLogger().info("Sending response to the sender...");
                    socket.getWriter().writeUTF("FAILED");
                    IBCLogger.getLogger().info("- SUCCESS");
                } catch (IOException e) {
                    IBCLogger.getLogger().info("- FAILED");
                    IBCLogger.getLogger().error("An error occurred while sending response to the sender", e);
                }
                return;
            }
            msg.setSender(user);
            try {
                Main.getInstance().getConfig().getUserConfig().registerUser(user);
            } catch (IOException e) {
                IBCLogger.getLogger().error("An error occurred while saving the user", e);
                try {
                    IBCLogger.getLogger().info("Sending response to the sender...");
                    socket.getWriter().writeUTF("FAILED");
                    IBCLogger.getLogger().info("- SUCCESS");
                } catch (IOException ex) {
                    IBCLogger.getLogger().info("- FAILED");
                    IBCLogger.getLogger().error("An error occurred while sending response to the sender", ex);
                }
                return;
            }
            IBCLogger.getLogger().info("- SUCCESS");
            try {
                IBCLogger.getLogger().info("Sending response to the sender...");
                socket.getWriter().writeUTF("SUCCESS");
                IBCLogger.getLogger().info("- SUCCESS");
            } catch (IOException e) {
                IBCLogger.getLogger().info("- FAILED");
                IBCLogger.getLogger().error("An error occurred while sending response to the sender", e);
            }
            return;
        }

        switch (msg.getType()) {
            case POST:
                switch (msg.getRequest()) {
                    case NAME:
                        try {
                            IBCLogger.getLogger().info("Updating name...");
                            if (msg.getArgs().length != 1)
                                throw new MessageBadlyFormattedException(message);
                            msg.getSender().setName(msg.getArgs()[0]);
                            IBCLogger.getLogger().info("- SUCCESS");
                            IBCLogger.getLogger().info("Saving user...");
                            msg.getSender().saveUser();
                            IBCLogger.getLogger().info("- SUCCESS");
                        } catch (MessageBadlyFormattedException | IOException ex) {
                            IBCLogger.getLogger().info("- FAILED");
                            IBCLogger.getLogger().error(ex.getMessage(), ex);
                        }
                        break;
//                    case CODE:
//                        try {
//                            IBCLogger.getLogger().info("Updating secret key...");
//                            if (msg.getArgs().length != 1)
//                                throw new MessageBadlyFormattedException(message);
//                            msg.getSender().setKeys(msg.getArgs()[0]);
//                            IBCLogger.getLogger().info("- SUCCESS");
//                            IBCLogger.getLogger().info("Saving user...");
//                            msg.getSender().saveUser();
//                            IBCLogger.getLogger().info("- SUCCESS");
//                        } catch (MessageBadlyFormattedException | IOException ex) {
//                            IBCLogger.getLogger().info("- FAILED");
//                            IBCLogger.getLogger().error(ex.getMessage(), ex);
//                        }
//                        break;
                    case TICKETS:
                        try {
                            IBCLogger.getLogger().info("Updating tickets...");
                            if (msg.getArgs().length != 2)
                                throw new MessageBadlyFormattedException(message);
                            char tag = msg.getArgs()[0].charAt(0);
                            switch (tag) {
                                case '+':
                                    msg.getSender().addTickets(Integer.parseInt(msg.getArgs()[1]));
                                    break;
                                case '=':
                                    msg.getSender().setTickets(Integer.parseInt(msg.getArgs()[1]));
                                    break;
                                default:
                                    throw new MessageBadlyFormattedException(message);
                            }
                            IBCLogger.getLogger().info("- SUCCESS");
                            IBCLogger.getLogger().info("Saving user...");
                            msg.getSender().saveUser();
                            IBCLogger.getLogger().info("- SUCCESS");
                        } catch (MessageBadlyFormattedException | IOException ex) {
                            IBCLogger.getLogger().info("- FAILED");
                            IBCLogger.getLogger().error(ex.getMessage(), ex);
                        }
                        break;
                }
                break;
            case GET:
                IBCLogger.getLogger().info("Obtaining information...");
                Message returnMsg = null;
                User user = msg.getSender();
                if (msg.getArgs().length == 2 && !msg.getArgs()[1].equalsIgnoreCase("all"))
                    try {
                        user = Main.getInstance().getConfig().getUserConfig().getUser(msg.getArgs()[1]);
                    } catch (NullPointerException ex) {
                        IBCLogger.getLogger().warn("Client not found in database.");
                        try {
                            sendResponse(socket, new Message(msg.getType(), msg.getRequest(), Main.LOCAL_USER, "404-NOT-FOUND"), null);
                        } catch (IOException e) {
                            IBCLogger.getLogger().info(" - FAILED");
                            IBCLogger.getLogger().error(e.getMessage(), e);
                            return;
                        }
                    }
                try {
                    switch (msg.getRequest()) {
                        case NAME:
                            returnMsg = new Message(msg.getType(), msg.getRequest(), Main.LOCAL_USER, user.getName());
                            break;
                        case TICKETS:
                            returnMsg = new Message(msg.getType(), msg.getRequest(), Main.LOCAL_USER, Integer.toString(user.getTickets()));
                            break;
                        case MONEY:
                            returnMsg = new Message(msg.getType(), msg.getRequest(), Main.LOCAL_USER, Integer.toString(user.getMoney()));
                            break;
//                        case CODE:
//                            StringBuilder args = new StringBuilder();
//                            if (msg.getArgs().length == 1 && msg.getArgs()[0].equalsIgnoreCase("all")) {
//                                for (User u : Main.getInstance().getConfig().getUserConfig().getClients())
//                                    args.append(u.getId()).append('#').append(u.getPrivateKey()).append("§");
//                                args.substring(0, args.length() - 1);
//                            } else if (user != null) {
//                                args.append(user.getId()).append('#').append(user.getPrivateKey());
//                            }
//                            IBCLogger.getLogger().debug("=> Message created : " + args.toString());
//                            returnMsg = new Message(msg.getType(), msg.getRequest(), Main.LOCAL_USER, args.toString());
//                            break;
                        case QUERY:
                            try {
                                String query = msg.getArgs()[0];
                                String id = query.split(";")[1];
                                IBCLogger.getLogger().info("Decrypting message...");
                                String decrypt;
                                try {
                                    decrypt = CryptoHandler.decrypt(query.split(";")[0], Main.getInstance().getConfig().getKeyConfig().getPrivateKey(id));
                                    IBCLogger.getLogger().debug("=> Decrypted message : " + decrypt);
                                    IBCLogger.getLogger().info("- SUCCESS");
                                } catch (Exception ex){
                                    IBCLogger.getLogger().info("- FAILED");
                                    IBCLogger.getLogger().error("An error occurred while decrypting the message", ex);
                                    return;
                                }
                                IBCLogger.getLogger().info("Parsing to packet...");
                                IBCClientPacket packet = IBCClientPacket.parse(decrypt);
                                User target = Main.getInstance().getConfig().getUserConfig().getUser(packet.getUserId());
                                IBCLogger.getLogger().info("Querying...");
                                switch (packet.getType()) {
                                    case M:
                                        if (target.getMoney() >= packet.getAmount()) {
                                            target.setMoney(target.getMoney() - packet.getAmount());
                                            target.setResult(true);
                                        } else {
                                            target.setResult(false);
                                        }
                                        break;
                                    case D:
                                        if (target.getTickets() >= packet.getAmount()) {
                                            target.addTickets(-packet.getAmount());
                                            target.setResult(true);
                                        } else {
                                            target.setResult(false);
                                        }
                                        break;
                                    default:
                                        target.setResult(false);
                                        break;
                                }
                                target.saveUser();
                                returnMsg = new Message(Message.Type.POST, Message.Request.QUERY, Main.LOCAL_USER, target.toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;
                    }
                } catch (NullPointerException ex) {
                    IBCLogger.getLogger().info("- FAILED");
                    IBCLogger.getLogger().error("The targeted user isn't precised.", ex);
                    throw new MessageBadlyFormattedException(message);
                }
                IBCLogger.getLogger().info("- SUCCESS");
                try {
                    if (returnMsg != null)
                        sendResponse(socket, returnMsg, msg.getSender().getType() == User.Type.CLIENT ? msg.getSender() : null);
                } catch (IOException e) {
                    IBCLogger.getLogger().info(" - FAILED");
                    IBCLogger.getLogger().error(e.getMessage(), e);
                    return;
                }
                break;
        }
        return;
    }

    public static void sendResponse(ConnectionManager.Client socket, Message message, User client) throws IOException {
        IBCLogger.getLogger().info("Sending response...");
        socket.getWriter().writeUTF(client == null ? message.parse() : message.parse());
        IBCLogger.getLogger().info(" - SUCCESS");
    }

}
