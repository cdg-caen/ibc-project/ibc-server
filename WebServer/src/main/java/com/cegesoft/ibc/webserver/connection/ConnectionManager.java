package com.cegesoft.ibc.webserver.connection;

import com.cegesoft.ibc.webserver.Main;
import com.cegesoft.ibc.webserver.exceptions.LowPermissionException;
import com.cegesoft.ibc.webserver.exceptions.MessageBadlyFormattedException;
import com.cegesoft.ibc.webserver.logging.IBCLogger;
import com.cegesoft.ibc.webserver.message.MessageManager;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class ConnectionManager implements Runnable {

    private ServerSocket server;

    private HashMap<String, Client> clients = new HashMap<>();

    public ConnectionManager() throws IOException {
        int port = Main.getInstance().getConfig().getPort();
        IBCLogger.getLogger().info("Starting server socket on port " + port);
        server = new ServerSocket(port);
        new Thread(this, "Connection-Thread").start();
    }

    @Override
    public void run() {
        IBCLogger.getLogger().info("Waiting connections...");
        while (Main.RUNNING) {
            try {
                Socket socket = server.accept();
                Client client = new Client(socket);
                clients.put(socket.getInetAddress().getHostAddress(), client);
                new Thread(client, "Connection - " + socket.getInetAddress().getHostAddress()).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void removeClient(String host){
        clients.remove(host);
    }

    public Client getClient(String host){
        return clients.get(host);
    }


    public static class Client implements Runnable {

        private DataInputStream reader;
        private DataOutputStream writer;
        private final Socket socket;

        public Client(Socket socket){
            this.socket = socket;
            IBCLogger.getLogger().info("======= Connected with " + socket.getInetAddress().getHostAddress() + " =======");
        }

        public void run() {
            try {
                IBCLogger.getLogger().info("Opening stream...");
                reader = new DataInputStream(socket.getInputStream());
                writer = new DataOutputStream(socket.getOutputStream());
                IBCLogger.getLogger().info("- SUCCESS");

                String message = waitForEntry();

                if (message.equals("")){
                    IBCLogger.getLogger().warn("Disconnecting the device...");
                    reader.close();
                    socket.close();
                    return;
                }

                try {
                    MessageManager.query(message, this);
                } catch (MessageBadlyFormattedException ex){
                    IBCLogger.getLogger().info("Analysis : FAILED");
                    IBCLogger.getLogger().error(ex.getMessage(), ex);
                } catch (LowPermissionException ex){
                    IBCLogger.getLogger().info("Checking permission : FAILED");
                    IBCLogger.getLogger().error(ex.getMessage(), ex);
                }

                IBCLogger.getLogger().info("Disconnecting the device...");
                try {
                    reader.close();
                    writer.close();
                    socket.close();
                } catch (IOException ex){
                    IBCLogger.getLogger().info("- FAILED");
                    IBCLogger.getLogger().error(ex.getMessage(), ex);
                }
                IBCLogger.getLogger().info("- SUCCESS");
                IBCLogger.getLogger().info("-------------------------");
            } catch (IOException e) {
                IBCLogger.getLogger().info("- FAILED");
                IBCLogger.getLogger().error(e.getMessage(), e);
            }
            IBCLogger.getLogger().info(" ");
            IBCLogger.getLogger().info(" ");
            Main.getInstance().getConnectionManager().removeClient(this.socket.getInetAddress().getHostAddress());
        }

        public String waitForEntry() {
            int i = 0;
            while (true){
                String message;
                try {
                    IBCLogger.getLogger().info("Receipt of data...");
                    while ((message = reader.readUTF()).equals("")){
                        IBCLogger.getLogger().debug(" => Received NULL");
                    }
                    IBCLogger.getLogger().debug("=> Received '" + message + "'");
                    IBCLogger.getLogger().info("- SUCCESS");
                    return message;
                } catch (IOException ex){
                    IBCLogger.getLogger().info("- FAILED");
                    IBCLogger.getLogger().error(ex.getMessage(), ex);
                    i++;
                    if (i < 5) {
                        IBCLogger.getLogger().info("Retrying reading...");
                        IBCLogger.getLogger().debug("=> " + i + " attempt" + (i > 1 ? "s" : ""));
                        continue;
                    }
                    return "";
                }

            }
        }

        public DataOutputStream getWriter() {
            return writer;
        }
    }

}
