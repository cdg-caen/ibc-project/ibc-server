package com.cegesoft.ibc.webserver.message;

import com.cegesoft.ibc.webserver.Main;
import com.cegesoft.ibc.webserver.connection.ConnectionManager;
import com.cegesoft.ibc.webserver.exceptions.MessageBadlyFormattedException;
import com.cegesoft.ibc.webserver.logging.IBCLogger;
import com.cegesoft.ibc.webserver.user.User;
import com.cegesoft.ibc.webserver.utils.AES;
import com.cegesoft.ibc.webserver.utils.CryptoHandler;
import org.apache.commons.codec.binary.Base64;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class Message {

    private final Type type;
    private User sender;
    private final Request request;
    private final String[] args;

    public Message(String message, ConnectionManager.Client socket) throws MessageBadlyFormattedException {
        if (!message.contains(":") && message.contains(";") && message.split(";").length == 2){
            IBCLogger.getLogger().info("Encrypted message received...");
            String id = message.split(";")[1];
            IBCLogger.getLogger().info("Getting the decryption key...");
            String key = Main.getInstance().getConfig().getKeyConfig().getPrivateKey(id);
            if (key != null){
                IBCLogger.getLogger().info("- SUCCESS");
                try {
                    IBCLogger.getLogger().info("Decrypting the message...");
                    message = CryptoHandler.decrypt(message.split(";")[0], key);
                    IBCLogger.getLogger().debug("=> Decrypted message : " + message);
                    IBCLogger.getLogger().info("- SUCCESS");
                } catch (Exception e) {
                    IBCLogger.getLogger().info("- FAILED");
                    IBCLogger.getLogger().error("An error occurred while decrypting the message", e);
                }
            } else
                IBCLogger.getLogger().info("- FAILED");
        }
        if (!message.contains(":") || message.split(":").length < 3){
            throw new MessageBadlyFormattedException(message);
        }
        String[] split = message.split(":");

        try {
            type = Type.valueOf(split[0].toUpperCase());
        } catch (Exception ex){
            throw new MessageBadlyFormattedException(message, ex);
        }

        try {
            request = Request.valueOf(split[1].toUpperCase());
        } catch (Exception ex){
            throw new MessageBadlyFormattedException(message, ex);
        }

        String id = split[2];
        try {
            sender = Main.getInstance().getConfig().getUserConfig().getUser(id);
        } catch (NullPointerException ignored){
            if (!request.equals(Request.DECLARE)) {
                try {
                    Main.getInstance().getConfig().getUserConfig().updateUser(sender = declareUser(message, socket));
                } catch (IOException e) {
                    IBCLogger.getLogger().error("An error occurred while updating user file.", e);
                }
            } else {
                sender = new User(id, "", "", "", 0, 0, User.Type.UNKNOWN);
            }
        }

        args = new String[split.length-3];

        System.arraycopy(split, 3, args, 0, split.length - 3);
    }

    public Message(Type type, Request request, User user, String... args){
        this.type = type;
        this.request = request;
        this.sender = user;
        this.args = args;
    }

    @Override
    public String toString() {
        return "Message[type=" + type.name() + ", request=" + request.name() + "]";
    }

    public String parse(){
        StringBuilder args = new StringBuilder();
        if (this.args.length >= 1)
            args.append(this.args[0]);
        if (this.args.length >= 2)
            for (int i = 1; i < this.args.length; i++)
                args.append(":").append(this.args[i]);
        return type.name() + ":" + request.name() + ":" + sender.getId() + ":" + args.toString();
    }

    public Type getType() {
        return type;
    }

    public User getSender() {
        return sender;
    }

    public String[] getArgs() {
        return args;
    }

    public Request getRequest() {
        return request;
    }

    private User declareUser(String message, ConnectionManager.Client socket) throws MessageBadlyFormattedException {
        IBCLogger.getLogger().info("Unknown sender, asking to declare himself...");
        try {
            MessageManager.sendResponse(socket, new Message(Type.GET, Request.DECLARE, Main.LOCAL_USER), null);
            IBCLogger.getLogger().info("Request sent, waiting for answer...");
        } catch (IOException e) {
            throw new MessageBadlyFormattedException(message, e);
        }
        String answer = socket.waitForEntry();
        IBCLogger.getLogger().info("Answer received. Generating user...");
        return getDeclaredUser(answer);
    }

    public User getDeclaredUser(String answer){
        if (!answer.contains("#") || (answer.split("#").length != 6 && answer.split("#").length != 3))
            return null;
        String[] split = answer.split("#");
        // userId#name#type#privateKey#publicKey#id
        String id = split[0];
        try {
            Main.getInstance().getConfig().getUserConfig().getUser(id);
        }catch (NullPointerException ex){
            String name = split[1];
            User.Type type = User.Type.valueOf(split[2].toUpperCase());
            if (type.equals(User.Type.CLIENT)) {
                String privateKey = split[3];
                String publicKey = split[4];
                Main.getInstance().getConfig().getKeyConfig().setKeys(split[5], privateKey, publicKey);
                return new User(id, name, privateKey, publicKey, 3, 150, type);
            }
            return new User(id, name, "", "", 0, 0, type);
        }
        return null;
    }

    void setSender(User user) {
        this.sender = user;
    }

    public enum Type {
        GET,
        POST;

        @Override
        public String toString() {
            return name();
        }
    }

    public enum Request {
        MONEY,
        TICKETS,
        NAME,
        CODE,
        DECLARE,
        QUERY;

        @Override
        public String toString() {
            return name();
        }
    }

}
