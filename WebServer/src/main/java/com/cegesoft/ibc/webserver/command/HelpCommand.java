package com.cegesoft.ibc.webserver.command;

import com.cegesoft.ibc.webserver.logging.IBCLogger;
import com.cegesoft.ibc.webserver.user.User;

import java.util.Map;

public class HelpCommand implements CommandExecutor {
    @Override
    public void execute(User sender, String command, String[] args) {
        IBCLogger.getLogger().info("List of commands : ");
        for (Map.Entry<String, CommandExecutor> executor : CommandHandler.getCommands().entrySet()){
            IBCLogger.getLogger().info(" - " + executor.getKey());
        }
        IBCLogger.getLogger().info("------------------");
    }
}
