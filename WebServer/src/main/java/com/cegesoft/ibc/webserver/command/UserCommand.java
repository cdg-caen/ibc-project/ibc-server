package com.cegesoft.ibc.webserver.command;

import com.cegesoft.ibc.webserver.Main;
import com.cegesoft.ibc.webserver.user.User;

import java.io.IOException;

import static com.cegesoft.ibc.webserver.logging.IBCLogger.getLogger;

public class UserCommand implements CommandExecutor {
    @Override
    public void execute(User sender, String command, String[] args) {
        if (args.length > 0) {
            switch (args[0].toLowerCase()) {
                case "list":
                    getLogger().info("List of users :");
                    getLogger().info("    Clients : ");
                    if (Main.getInstance().getConfig().getUserConfig().getClients().isEmpty())
                        getLogger().info("    - No Client...");
                    else
                        for (User user : Main.getInstance().getConfig().getUserConfig().getClients())
                            getLogger().info("    - " + user.getName() + " (" + user.getId() + ")");
                    getLogger().info(" ");
                    getLogger().info("    Terminals : ");
                    if (Main.getInstance().getConfig().getUserConfig().getTerminals().isEmpty())
                        getLogger().info("    - No Terminal...");
                    else
                        for (User user : Main.getInstance().getConfig().getUserConfig().getTerminals())
                            getLogger().info("    - " + user.getName() + " (" + user.getId() + ")");
                    getLogger().info("----------------");
                    break;
                case "info":
                    if (args.length == 2){
                        User user;
                        try {
                            user = Main.getInstance().getConfig().getUserConfig().getUser(args[1]);
                            if (user == null)
                                throw new NullPointerException();
                        } catch (NullPointerException ex){
                            getLogger().info("The user who has the ID '" + args[1] + "' doesn't exist.");
                            getLogger().error("An error occurred while getting User '" + args[1] + "'", ex);
                            return;
                        }
                        getLogger().info("User information : " + user.getName() + " (" + user.getId() + ")");
                        getLogger().info(" - Type : " + user.getType());
                        getLogger().info(" - Secret key : " + user.getPrivateKey());
                        getLogger().info(" - Money : " + user.getMoney() + " €");
                        getLogger().info(" - Tickets : " + user.getTickets());
                        getLogger().info("----------------------");
                    } else {
                        getLogger().info("Please use 'user info [USER_ID]'");
                    }
                    break;
                case "delete":
                    if (args.length == 2){
                        User user;
                        try {
                            user = Main.getInstance().getConfig().getUserConfig().getUser(args[1]);
                            if (user == null)
                                throw new NullPointerException();
                        } catch (NullPointerException ex){
                            getLogger().info("The user who has the ID '" + args[1] + "' doesn't exist.");
                            getLogger().error("An error occurred while getting User '" + args[1] + "'", ex);
                            return;
                        }
                        getLogger().info("Deleting user...");
                        try {
                            Main.getInstance().getConfig().getUserConfig().removeUser(user.getId());
                        } catch (IOException e) {
                            getLogger().info("Deleting failed...");
                            getLogger().error("An error occurred while deleting user '" + user.getId() + "'", e);
                            return;
                        }
                        getLogger().info("User successfully deleted");
                    } else {
                        getLogger().info("Please use 'user delete [USER_ID]'");
                    }
                    break;
            }
        } else {
            getLogger().info("User command must have at least one argument : list, info [id], delete [id]");
        }
    }
}
