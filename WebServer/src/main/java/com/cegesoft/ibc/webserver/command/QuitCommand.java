package com.cegesoft.ibc.webserver.command;

import com.cegesoft.ibc.webserver.user.User;

public class QuitCommand implements CommandExecutor {
    @Override
    public void execute(User sender, String command, String[] args) {
        System.exit(0);
    }
}
