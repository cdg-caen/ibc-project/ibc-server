package com.cegesoft.ibc.webserver.utils;

public enum IBCPacketType {
    M,
    U,
    D;
}
