package com.cegesoft.ibc.webserver.utils;

import com.cegesoft.ibc.webserver.exceptions.MessageBadlyFormattedException;

public class IBCClientPacket {

    private final IBCPacketType type;
    private final String token;
    private final String userId;
    private final int amount;

    public IBCClientPacket(IBCPacketType type, String token, String userId, int amount){
        this.type = type;
        this.token = token;
        this.userId = userId;
        this.amount = amount;
    }

    public static IBCClientPacket parse(String protocol) throws MessageBadlyFormattedException {
        if (!protocol.contains(":"))
            throw new MessageBadlyFormattedException(protocol);
        protocol = protocol.replace("#", "");
        String[] split = protocol.split(":");
        IBCPacketType type = IBCPacketType.valueOf(split[0].toUpperCase());
        String userId = split[1];
        String token = split[2];
        int amount = Integer.parseInt(split[3]);
        return new IBCClientPacket(type, token, userId, amount);
    }

    public IBCPacketType getType() {
        return type;
    }

    public String getToken() {
        return token;
    }

    public String getUserId() {
        return userId;
    }

    public int getAmount() {
        return amount;
    }
}
