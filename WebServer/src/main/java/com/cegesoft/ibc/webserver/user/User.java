package com.cegesoft.ibc.webserver.user;

import com.cegesoft.ibc.webserver.Main;
import com.cegesoft.ibc.webserver.message.Message;

import java.io.IOException;

public class User {

    private final String id;
    private final Type type;
    private String name;
    private final String privateKey;
    private final String publicKey;
    private int tickets;
    private int money;
    private boolean result;

    public User(String id, String name, String privateKey, String publicKey, int tickets, int money, Type type) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.tickets = tickets;
        this.money = money;
        this.privateKey = privateKey;
        this.publicKey = publicKey;
    }

    public void addTickets(int tickets) {
        this.tickets += tickets;
    }

    public int getTickets() {
        return this.tickets;
    }

    public void setTickets(int tickets) {
        this.tickets = tickets;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Type getType() {
        return type;
    }

    public void saveUser() throws IOException {
        Main.getInstance().getConfig().getUserConfig().updateUser(this);
    }

    @Override
    public String toString() {
        return this.name + "@" + this.money + "@" + this.tickets + "@" + this.result;
    }

    public boolean hasPermission(Message message) {
        return hasPermission(message.getRequest(), message.getType());
    }

    public boolean hasPermission(Message.Request request, Message.Type type) {
        if (request.equals(Message.Request.DECLARE))
            return true;
        if (getType().equals(Type.SERVER))
            return true;
        switch (type) {
            case GET:
                switch (request) {
                    case NAME:
                    case QUERY:
                    case MONEY:
                    case TICKETS:
                        return getType().equals(Type.CLIENT) || getType().equals(Type.TERMINAL);
                }
                break;
            case POST:
                switch (request) {
                    case NAME:
                        return getType().equals(Type.CLIENT);
                    case MONEY:
                        return false;
                    case TICKETS:
                        return getType().equals(Type.CLIENT) || getType().equals(Type.TERMINAL);
                }
                break;
        }
        return false;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public enum Type {
        TERMINAL,
        SERVER,
        CLIENT, UNKNOWN;

        @Override
        public String toString() {
            return name();
        }
    }
}
