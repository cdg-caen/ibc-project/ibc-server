package com.cegesoft.ibc.webserver.config;

import com.cegesoft.ibc.webserver.user.User;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserConfig extends Config {

    UserConfig() {
        super("users");
    }

    public User getUser(String id) throws NullPointerException {
        if (!this.contains(id))
            throw new NullPointerException("Id Not found in database.");
        JSONObject object = this.get(id);
        assert object != null : "User object == null";
        if (!object.containsKey("name") || !object.containsKey("type") || !object.containsKey("tickets") || !object.containsKey("money"))
            throw new NullPointerException("User save is corrupted. Please purge the file.");
        String name = (String) object.get("name");
        User.Type type;
        try {
            type = User.Type.valueOf((String) object.get("type"));
        } catch (Exception ex){
            throw new NullPointerException("Type is null");
        }
        int tickets = 0;
        int money = 0;
        if (type.equals(User.Type.CLIENT)) {
            Object ticketsObj = object.get("tickets");
            if (ticketsObj instanceof Long)
                tickets = Integer.parseInt(Long.toString((Long)ticketsObj));
            else
                tickets = (Integer)ticketsObj;

            Object moneyObj = object.get("money");
            if (ticketsObj instanceof Long)
                money = Integer.parseInt(Long.toString((Long)moneyObj));
            else
                money = (Integer)moneyObj;
        }
        return new User(id, name, "", "", tickets, money, type);
    }

    public List<User> getClients(){
        return ((Stream<String>)this.json.keySet().stream()).filter(str -> getUser(str).getType().equals(User.Type.CLIENT)).map(str -> getUser(str)).collect(Collectors.toList());
    }

    public List<User> getTerminals(){
        return ((Stream<String>)this.json.keySet().stream()).filter(str -> getUser(str).getType().equals(User.Type.TERMINAL)).map(str -> getUser(str)).collect(Collectors.toList());
    }


    public void updateUser(User user) throws IOException {
        JSONObject object = new JSONObject();
        object.put("name", user.getName());
        object.put("type", user.getType().toString());
        if (user.getType().equals(User.Type.CLIENT)) {
            object.put("tickets", user.getTickets());
            object.put("money", user.getMoney());
        }
        json.replace(user.getId(), object);
        saveConfig();
    }

    public void registerUser(User user) throws IOException {
        JSONObject object = new JSONObject();
        object.put("name", user.getName());
        object.put("type", user.getType().toString());
        if (user.getType().equals(User.Type.CLIENT)) {
            object.put("tickets", user.getTickets());
            object.put("money", user.getMoney());
        }
        json.put(user.getId(), object);
        saveConfig();
    }

    public void removeUser(String id) throws IOException {
        this.json.remove(id);
        saveConfig();
    }

    @Override
    protected JSONObject generateDefaults(JSONObject object) {
        return object;
    }
}
