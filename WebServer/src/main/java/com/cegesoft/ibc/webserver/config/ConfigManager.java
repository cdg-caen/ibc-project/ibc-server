package com.cegesoft.ibc.webserver.config;

import com.cegesoft.ibc.webserver.Main;
import com.cegesoft.ibc.webserver.logging.IBCLogger;
import com.cegesoft.ibc.webserver.user.User;
import org.json.simple.JSONObject;

public class ConfigManager extends Config {

    private final UserConfig userConfig;
    private final KeyConfig keyConfig;

    public ConfigManager() {
        super("config");
        this.setLoggerLevels();
        this.userConfig = new UserConfig();
        this.keyConfig = new KeyConfig();
        Main.LOCAL_USER = new User("0001", "Server", "", "", 0, 0, User.Type.SERVER);
    }

    public int getPort() {
        return ((Long)json.getOrDefault("port", 35000)).intValue();
    }

    private void setLoggerLevels(){
        IBCLogger.DEBUG = (Boolean)json.getOrDefault("debug_level", false);
        IBCLogger.WARN = (Boolean)json.getOrDefault("warn_level", true);
        IBCLogger.ERROR = (Boolean)json.getOrDefault("error_level", true);
    }

    public UserConfig getUserConfig() {
        return userConfig;
    }

    @Override
    protected JSONObject generateDefaults(JSONObject object) {
        object.put("port", 35000);
        object.put("debug_level", false);
        object.put("warn_level", true);
        object.put("error_level", true);
        return object;
    }

    public KeyConfig getKeyConfig() {
        return keyConfig;
    }
}
