package com.cegesoft.ibc.webserver.utils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import static java.nio.charset.StandardCharsets.UTF_8;

public class RSA {
    public static String privateKey = "MIIBVQIBADANBgkqhkiG9w0BAQEFAASCAT8wggE7AgEAAkEAv3IJ9lUu99vFSV7ufjOt2pOm168SsGph7DYenhpqVshJJ3qPNIMxyXsj1KbnDz0T41ojqwv4iWSFGbhrw+rRaQIDAQABAkB+yzuX31ENLYwbjPktsnKJmlLc9IHPP5X9SNa8ZchaEoVEjoNCLGdCu1VQuRGEMDLcVgqj3JtZJ2EdbowcooZFAiEA4ASEhGNUqbGQwN6BobgN1CnXquKawypAQGnA7V2fwscCIQDaxw+LyIUKPg9Xv8s4QGQF/JBbDtUo243sIYWSZf6aTwIhALxx0G/1hxDAuD4mbvH1fFyPXkb4HKw0bJxC2fJTIcOdAiBUi0R4sj6nXKA1OsrfrBJ7NcTbS3oYVfAcZj/l+UBQhQIhAKgnJCm/AO9EzCGVeMrnvpvXPUGP2GCM/d8hSKHBpMng";
    public static String publicKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAL9yCfZVLvfbxUle7n4zrdqTptevErBqYew2Hp4aalbISSd6jzSDMcl7I9Sm5w89E+NaI6sL+IlkhRm4a8Pq0WkCAwEAAQ";


    private static PublicKey getPublicKey(String base64PublicKey){
        PublicKey publicKey = null;
        try{
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(base64PublicKey.getBytes()));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            publicKey = keyFactory.generatePublic(keySpec);
            return publicKey;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return publicKey;
    }

    private static PrivateKey getPrivateKey(String base64PrivateKey){
        PrivateKey privateKey = null;
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(base64PrivateKey.getBytes()));
        KeyFactory keyFactory = null;
        try {
            keyFactory = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            privateKey = keyFactory.generatePrivate(keySpec);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return privateKey;
    }

    public static String encrypt(String data, String publicKey) throws BadPaddingException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, getPublicKey(publicKey));
        return Base64.getEncoder().encodeToString(cipher.doFinal(data.getBytes()));
    }

    private static String decrypt(byte[] data, PrivateKey privateKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return new String(cipher.doFinal(data));
    }

    public static String decrypt(String data, String base64PrivateKey) {
        try {
            return decrypt(Base64.getDecoder().decode(data.getBytes()), getPrivateKey(base64PrivateKey));
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return "";
    }
}
