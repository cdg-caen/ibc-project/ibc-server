package com.cegesoft.ibc.webserver.exceptions;

import com.cegesoft.ibc.webserver.message.Message;
import com.cegesoft.ibc.webserver.user.User;
import org.apache.commons.lang3.StringUtils;

public class LowPermissionException extends RuntimeException {

    public LowPermissionException(User user, Message message){
        super("The " + StringUtils.capitalize(user.getType().toString().toLowerCase()) + " " + user.getName() + " (" + user.getId() + ") tried this request : " + message);
    }

}
