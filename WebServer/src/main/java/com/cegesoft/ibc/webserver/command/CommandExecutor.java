package com.cegesoft.ibc.webserver.command;

import com.cegesoft.ibc.webserver.user.User;

public interface CommandExecutor {

    void execute(User sender, String command, String[] args);
}
