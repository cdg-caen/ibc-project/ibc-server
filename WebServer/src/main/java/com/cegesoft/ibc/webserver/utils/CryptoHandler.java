package com.cegesoft.ibc.webserver.utils;

import com.cegesoft.ibc.webserver.logging.IBCLogger;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

public class CryptoHandler {

    public static String encrypt(String data, String publicKey) {
        try {
            SecretKey AESKey = AES.generateKey();
            String AESKeyString = Base64.encodeBase64String(AESKey.getEncoded());
            String crypted = Base64.encodeBase64String(AES.encrypt(data, AESKey));
            return crypted + "!" + RSA.encrypt(AESKeyString, publicKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String decrypt(String message, String privateKey) {
        try {
            String crypted = message.split("!")[0];
            String cryptedKey = message.split("!")[1];

            String key = RSA.decrypt(cryptedKey, privateKey);
            return new String(AES.decrypt(Base64.decodeBase64(crypted), key), StandardCharsets.UTF_8);
        } catch (Exception e) {
            IBCLogger.getLogger().error("Can't decrypt message '" + message + "' with Key : " + privateKey);
            e.printStackTrace();
        }
        return "";
    }

//    public static void main(String[] strings) {
//        String privateKey = "MIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEAqqZTiZ7hFYeIQEUFFQ7Gb8DsqWZmeB5Ca2rc9DL6bxWkUauZ4ERQsP88mRmJe6iTzLtOW5apTgFKU6GIcDYBIQIDAQABAkBJV9kZke59eVXHAaMQjNweldVhbUBnKeI0FqbGUc6uxPNGPGWnDKlDj9dKNkUzm4sFQZZ+WXIPDHtqZMIBO5zdAiEA4QMH+J+7D7pi80YV4rysoQMNcJdCXNNhvOBtqnTQMg8CIQDCJrc53VlO6+Tepnq1etb8/LKDSjug4XvSIlI14zUJzwIhAJ/PGVA5kg//ntuNQWoB+tF4a4k1xMpMzzPoabsQr/V9AiAYYxSQyCT3rNnKvc6cDRoEHRmvNBvZUXlkI7t/2VJIMQIgdaykBHR4clQ3ZPw/60bpRjwe72QZp/zUHBiI2jEZ91s";
//        String encrypt = "X0q2SgCbhVxvW2Do952zrYv3l0Esy861jsa5Y9Rwqzs!RE2VdBY+C7LDxKrbWhwXkLRGxKCfw+hLiEml6YbomtbPkLG7frM3TqCzysCybOcYZa7OH72aBW3jXolPgCxpXg";
//        System.out.println(decrypt(encrypt, privateKey));
//    }

}
