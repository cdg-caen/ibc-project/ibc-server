package com.cegesoft.ibc.webserver.command;

import com.cegesoft.ibc.webserver.Main;
import com.cegesoft.ibc.webserver.exceptions.LowPermissionException;
import com.cegesoft.ibc.webserver.logging.IBCLogger;
import com.cegesoft.ibc.webserver.message.Message;
import com.cegesoft.ibc.webserver.user.User;

import java.io.IOException;

/**
 * Created by HoxiSword on 27/09/2020 for IBC
 */
public class RegisterCommand implements CommandExecutor {
    @Override
    public void execute(User sender, String command, String[] args) {
        if (args.length == 1) {
            Message msg = new Message(args[0], null);
            IBCLogger.getLogger().info("Analysis : SUCCESS");
            IBCLogger.getLogger().debug(" ");
            IBCLogger.getLogger().debug("-- Message information --");
            IBCLogger.getLogger().debug("=> TYPE = " + msg.getType());
            IBCLogger.getLogger().debug("=> REQUEST = " + msg.getRequest());
            IBCLogger.getLogger().debug("=> SENDER = " + msg.getSender().getType() + " (" + msg.getSender().getId() + ")");
            IBCLogger.getLogger().debug("=> ARGS_LENGTH = " + msg.getArgs().length);
            IBCLogger.getLogger().debug(" ");
            if (!msg.getSender().hasPermission(msg))
                throw new LowPermissionException(msg.getSender(), msg);

            IBCLogger.getLogger().info("Checking permission : SUCCESS");

            if (msg.getRequest().equals(Message.Request.DECLARE)) {
                IBCLogger.getLogger().info("Declaring user...");
                User user = msg.getDeclaredUser(msg.getArgs()[0]);
                if (user == null) {
                    IBCLogger.getLogger().info("- FAILED");
                    return;
                }
                try {
                    Main.getInstance().getConfig().getUserConfig().registerUser(user);
                } catch (IOException e) {
                    IBCLogger.getLogger().error("An error occurred while saving the user", e);
                    return;
                }
                IBCLogger.getLogger().info("- SUCCESS");
            }
        }
    }
}
