package com.cegesoft.ibc.webserver.config;

import com.cegesoft.ibc.webserver.logging.IBCLogger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import static com.cegesoft.ibc.webserver.logging.IBCLogger.getLogger;

public abstract class Config {

    private final File file;
    protected JSONObject json;
    private String name;

    protected Config(String name) {
        this.name = name;
        getLogger().info("--- Loading '" + name + ".json' ---");
        this.file = new File(name + ".json");
        try {
            if (!file.exists()) {
                getLogger().warn("    File doesn't exist. Generating in process...");
                if (file.createNewFile()) {
                    json = generateDefaults(new JSONObject());
                    saveConfig();
                    getLogger().info("    File successfully generated");
                }
            }
            IBCLogger.getLogger().info("    Reading file...");
            json = (JSONObject) new JSONParser().parse(new FileReader(file));
            IBCLogger.getLogger().info("    File successfully read !");
            IBCLogger.getLogger().info("----------------------------");
            IBCLogger.getLogger().info(" ");
        } catch (Exception ex) {
            IBCLogger.getLogger().error("An error occurred while creating the config '" + name + "'", ex);
        }
    }

    protected abstract JSONObject generateDefaults(JSONObject object);

    protected void saveConfig() throws IOException {
        IBCLogger.getLogger().info("Saving into '" + name + ".json'...");
        FileWriter writer = new FileWriter(file);
        writer.write(json.toString());
        writer.flush();
        writer.close();
        IBCLogger.getLogger().info("File saved.");
    }

    protected <T> T get(String path) {
        return (T) json.get(path);
    }

    protected boolean contains(String path) {
        return json.containsKey(path);
    }


}
