package com.cegesoft.ibc.webserver.command;

import com.cegesoft.ibc.webserver.Main;
import com.cegesoft.ibc.webserver.logging.IBCLogger;

import java.util.HashMap;
import java.util.Scanner;

public class CommandHandler implements Runnable {

    private static final HashMap<String, CommandExecutor> commands = new HashMap<>();

    public static void registerCommand(String command, CommandExecutor executor){
        if (commands.containsKey(command.toLowerCase()))
            return;
        commands.put(command.toLowerCase(), executor);
    }

    static HashMap<String, CommandExecutor> getCommands(){
        return commands;
    }

    private static void execute(String command, String[] args){
        if (!commands.containsKey(command.toLowerCase())){
            IBCLogger.getLogger().saveLine();
            IBCLogger.getLogger().info("Command not found. Use 'help' to see all commands");
            return;
        }
        commands.get(command.toLowerCase()).execute(Main.LOCAL_USER, command, args);
    }

    private static boolean INIT = false;

    public static void init(){
        if (!INIT){
            INIT = true;
            new Thread(new CommandHandler(), "Command-Thread").start();
        }
    }

    @Override
    public void run() {
        Scanner scanner = new Scanner(System.in);
        while (Main.RUNNING){
            String entry = scanner.nextLine();
            String command;
            String[] args;
            if (entry.contains(" ")) {
                command = entry.split(" ")[0];
                args = new String[entry.split(" ").length-1];
                System.arraycopy(entry.split(" "), 1, args, 0, entry.split(" ").length-1);
            } else {
                command = entry;
                args = new String[0];
            }
            execute(command, args);
        }
    }
}
