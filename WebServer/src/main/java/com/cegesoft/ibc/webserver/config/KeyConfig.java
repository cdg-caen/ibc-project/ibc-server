package com.cegesoft.ibc.webserver.config;

import org.json.simple.JSONObject;

import java.io.IOException;

import static com.cegesoft.ibc.webserver.logging.IBCLogger.getLogger;

public class KeyConfig extends Config {

    protected KeyConfig() {
        super("keys");
    }

    @Override
    protected JSONObject generateDefaults(JSONObject object) {
        return object;
    }

    public void setKeys(String id, String privateKey, String publicKey){
        if (json.containsKey(id))
            return;
        JSONObject keys = new JSONObject();
        keys.put("privateKey", privateKey);
        keys.put("publicKey", publicKey);
        this.json.put(id, keys);
        try {
            saveConfig();
        } catch (IOException e) {
            getLogger().error("An error occurred while saving the Key file.", e);
        }
    }

    public String getPrivateKey(String id){
        return ((String)((JSONObject)json.get(id)).get("privateKey")).replace("%", "/");
    }
    public String getPublicKey(String id){
        return ((String)((JSONObject)json.get(id)).get("publicKey")).replace("%", "/");
    }
}
