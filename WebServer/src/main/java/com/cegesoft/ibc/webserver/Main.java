package com.cegesoft.ibc.webserver;

import com.cegesoft.ibc.webserver.command.*;
import com.cegesoft.ibc.webserver.config.ConfigManager;
import com.cegesoft.ibc.webserver.connection.ConnectionManager;
import com.cegesoft.ibc.webserver.logging.IBCLogger;
import com.cegesoft.ibc.webserver.user.User;

import java.io.IOException;

public final class Main {

    public static final String VERSION = "1.0.3";
    public static boolean RUNNING = true;
    public static User LOCAL_USER;

    private static Main instance;
    private ConnectionManager connectionManager;
    private ConfigManager config;
    private IBCLogger logger;

    private Main() {
        instance = this;
    }

    private void init() {
        Thread.currentThread().setName("Main-Thread");
        this.logger = new IBCLogger();
        this.logger.info("=-=-=-=-=-=- STARTING SERVER (v" + VERSION + ") -=-=-=-=-=-=");
        this.logger.info(" ");
        this.config = new ConfigManager();
        try {
            this.connectionManager = new ConnectionManager();
        } catch (IOException e) {
            this.logger.error("An error occurred while enabling ConnectionManager.class", e);
        }
        this.logger.info(" ");
        this.logger.info("Initializing commands...");
        CommandHandler.init();
        this.logger.info("Registering commands...");
        CommandHandler.registerCommand("help", new HelpCommand());
        CommandHandler.registerCommand("user", new UserCommand());
        CommandHandler.registerCommand("quit", new QuitCommand());
        CommandHandler.registerCommand("register", new RegisterCommand());
    }

    public ConnectionManager getConnectionManager() {
        return connectionManager;
    }

    public ConfigManager getConfig() {
        return config;
    }

    public static Main getInstance() {
        return instance;
    }

    public static void main(String[] args) {
        new Main().init();
    }


    public IBCLogger getLogger() {
        return logger;
    }
}
