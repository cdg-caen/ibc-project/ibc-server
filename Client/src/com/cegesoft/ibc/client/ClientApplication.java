package com.cegesoft.ibc.client;

import com.cegesoft.ibc.client.config.ConfigManager;
import com.cegesoft.ibc.client.connection.bluetooth.BluetoothConnection;
import com.cegesoft.ibc.client.connection.serial.SerialHandler;
import com.cegesoft.ibc.client.exceptions.IncorrectPacketFormatException;
import com.cegesoft.ibc.client.gui.IBCFrame;
import com.cegesoft.ibc.client.gui.utils.BauhausFont;
import com.cegesoft.ibc.client.packets.IBCClientPacket;
import com.cegesoft.ibc.client.packets.IBCServerPacket;
import com.cegesoft.ibc.client.user.IBCUser;
import fr.theshark34.swinger.Swinger;

import java.io.IOException;

public final class ClientApplication {

    private static ClientApplication instance;

    private IBCUser user;
    private ConfigManager config;
    private IBCFrame frame;
    private boolean started;

    public void stop(){
        started = false;
    }

    public IBCUser getUser(){
        return this.user;
    }

    public static ClientApplication getClient(){
        return instance;
    }

    private ClientApplication(){
        instance = this;
        Swinger.setSystemLookNFeel();
        Swinger.setResourcePath("/com/cegesoft/ibc/resources/");
        new BauhausFont();
        this.config = new ConfigManager();
        this.user = this.config.getUser();
        this.frame = new IBCFrame();
        this.started = true;
        while (started) {
            SerialHandler serial = new SerialHandler();
            IBCServerPacket serverPacket = null;
            System.out.println("En attente d'une reception série...");
            while(started) {
                try {
                    serverPacket = new IBCServerPacket(serial.read());
                    break;
                } catch (IncorrectPacketFormatException e) {
                    e.printStackTrace();
                }
            }

            if (serverPacket == null)
                continue;

            System.out.println("Recu !");
            serial.disconnect();

            boolean authorized = this.user.authorized(serverPacket.getType());

            this.user.extractTicket(serverPacket.getType());

            BluetoothConnection bluetooth = new BluetoothConnection(serverPacket.getAddress());
            System.out.println("Tentative de connexion bluetooth : ");
            bluetooth.connect();

            IBCClientPacket packet = new IBCClientPacket(serverPacket.getType(), this.user, serverPacket.getToken(), authorized);
            bluetooth.writeCrypted(packet.toString());

            System.out.println("Envoi par bluetooth : " +packet.toString());

            try {
                bluetooth.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Restarting process...");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void exit(){
        this.config.saveUser();
        this.started = false;
        System.exit(0);
    }

    public static void main(String[] args) {
        new ClientApplication();
    }

    public ConfigManager getConfig() {
        return config;
    }
}
