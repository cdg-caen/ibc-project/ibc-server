package com.cegesoft.ibc.client.packets;

public enum IBCPacketType {

    M,
    D,
    U

}
