package com.cegesoft.ibc.client.packets;

import com.cegesoft.ibc.client.exceptions.IncorrectPacketFormatException;
import com.sun.istack.internal.NotNull;

public class IBCServerPacket {

    private final IBCPacketType type;
    private final String token;
    private final String address;

    public IBCServerPacket(@NotNull String packet) throws IncorrectPacketFormatException {
        if (!packet.contains(":")) {
            throw new IncorrectPacketFormatException(packet);
        }

        String[] split = packet.replace("#", "").split(":");

        if (split.length != 3) {
            throw new IncorrectPacketFormatException(packet);
        }

        String typeName = split[0];
        this.token = split[1];
        this.address = split[2];

        try {
            this.type = IBCPacketType.valueOf(typeName);
        } catch (Exception e) {
            throw new IncorrectPacketFormatException(packet);
        }
    }

    public IBCPacketType getType() {
        return type;
    }

    public String getToken() {
        return token;
    }

    public String getAddress() {
        return address;
    }
}
