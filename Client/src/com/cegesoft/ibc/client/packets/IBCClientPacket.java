package com.cegesoft.ibc.client.packets;

import com.cegesoft.ibc.client.user.IBCUser;

public class IBCClientPacket {

    private final IBCPacketType type;
    private final IBCUser user;
    private final String token;
    private final boolean authorized;

    public IBCClientPacket(IBCPacketType type, IBCUser user, String token, boolean authorized){
        this.type = type;
        this.user = user;
        this.token = token;
        this.authorized = authorized;
    }

    @Override
    public String toString() {
        return type.name() + ":" + user.toString(type) + ":" + token + ":" + authorized + "#";
    }
}
