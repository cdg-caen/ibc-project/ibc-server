package com.cegesoft.ibc.client.exceptions;

public class ClientConnectionStateException extends Exception {

    public ClientConnectionStateException(){
        this("Client isn't connected : ");
    }

    public ClientConnectionStateException(String msg){
        super(msg);
    }

}
