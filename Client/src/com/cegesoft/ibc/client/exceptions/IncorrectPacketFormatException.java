package com.cegesoft.ibc.client.exceptions;

public class IncorrectPacketFormatException extends Exception {

    public IncorrectPacketFormatException(String packet){
        super("IBCPacket incorrectly formatted : " + packet);
    }

    public IncorrectPacketFormatException(String packet, String message){
        super(message + " " + packet);
    }

}
