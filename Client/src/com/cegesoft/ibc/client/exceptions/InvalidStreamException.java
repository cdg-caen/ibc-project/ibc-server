package com.cegesoft.ibc.client.exceptions;

public class InvalidStreamException extends Exception {

    public InvalidStreamException(String msg){
        super(msg);
    }
}
