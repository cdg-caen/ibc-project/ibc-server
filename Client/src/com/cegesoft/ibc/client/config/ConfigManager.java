package com.cegesoft.ibc.client.config;

import com.cegesoft.ibc.client.ClientApplication;
import com.cegesoft.ibc.client.user.IBCID;
import com.cegesoft.ibc.client.user.IBCUser;
import com.cegesoft.ibc.client.packets.IBCPacketType;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class ConfigManager {

    private final File file;
    private JSONObject json;

    public ConfigManager() {
        this.file = new File("client.json");
        try {
            if (!file.exists()) {
                file.createNewFile();
                JSONObject object = new JSONObject();

                JSONObject userObj = new JSONObject();
                IBCUser user = new IBCUser(IBCID.create());
                user.setFirstName("");
                user.setName("");
                for (IBCPacketType type : IBCPacketType.values()){
                    user.setTicket(type, 3);
                }
                userObj.put("id", user.getId().toString());
                userObj.put("fname", user.getFirstName());
                userObj.put("name", user.getName());

                JSONObject tickets = new JSONObject();
                for (Map.Entry<IBCPacketType, Integer> ticket : user.getTickets().entrySet()){
                    tickets.put(ticket.getKey().name(), ticket.getValue());
                }

                userObj.put("tickets", tickets);

                object.put("user", userObj);

                // GLOBAL
                object.put("port", "");
                json = object;
                saveConfig();
            }
            System.out.println(file.getAbsolutePath());
            json = (JSONObject) new JSONParser().parse(new FileReader(file));
        } catch (Exception ignored) {
        }
    }

    private void saveConfig() {
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json.toString());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public IBCUser getUser() {
        JSONObject obj = (JSONObject) json.get("user");
        String id = (String) obj.get("id");
        IBCUser user = new IBCUser(IBCID.parse(id));
        user.setName((String) obj.get("name"));
        user.setFirstName((String) obj.get("fname"));

        JSONObject tickets = (JSONObject) obj.get("tickets");
        for (Object ticket : tickets.entrySet()){
            Map.Entry<String, Object> entry = (Map.Entry<String, Object>)ticket;
            IBCPacketType type;
            try {
                type = IBCPacketType.valueOf(entry.getKey());
            } catch (Exception e){
                continue;
            }
            user.setTicket(type, Integer.parseInt(((Long)entry.getValue()).toString()));
        }
        return user;
    }

    public void saveUser(){
        IBCUser user = ClientApplication.getClient().getUser();
        if (user == null)
            return;
        JSONObject userObj = new JSONObject();
        userObj.put("id", user.getId().toString());
        userObj.put("name", user.getName());
        userObj.put("fname", user.getFirstName());

        JSONObject tickets = new JSONObject();
        for (Map.Entry<IBCPacketType, Integer> ticket : user.getTickets().entrySet()){
            tickets.put(ticket.getKey().name(), ticket.getValue());
        }

        userObj.put("tickets", tickets);
        json.remove("user");
        json.put("user", userObj);
        saveConfig();
    }

    public String getPortName() {
        return (String) json.get("port");
    }

}
