package com.cegesoft.ibc.client.gui.utils;

import java.awt.*;
import java.io.IOException;

public class BauhausFont {

    public static Font BAUHAUS;

    public BauhausFont() {
        try {
            BAUHAUS = Font.createFont(Font.TRUETYPE_FONT, getClass().getResource("/com/cegesoft/ibc/client/gui/utils/bauhaus.ttf").openStream());


            GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
            genv.registerFont(BAUHAUS);
            BAUHAUS = BAUHAUS.deriveFont(35f);
        } catch (FontFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
