package com.cegesoft.ibc.client.gui;

import com.cegesoft.ibc.client.ClientApplication;

import javax.swing.*;
import java.awt.event.*;

public class IBCFrame extends JFrame {

    public IBCFrame(){
        this.setSize(500, 280);
        this.setTitle("Client IBC");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setFocusable(true);
        this.setUndecorated(false);
        this.setResizable(false);

        this.setContentPane(new IBCMainPanel());

        this.setVisible(true);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.out.println("Exit system...");
                IBCFrame.this.setVisible(false);
                ClientApplication.getClient().exit();
            }
        });
    }

}
