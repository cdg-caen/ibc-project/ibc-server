package com.cegesoft.ibc.client.gui;

import com.cegesoft.ibc.client.ClientApplication;
import com.cegesoft.ibc.client.gui.utils.BauhausFont;
import com.cegesoft.ibc.client.packets.IBCPacketType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class IBCMainPanel extends JPanel {

    private JLabel nomLabel, prenomLabel, title;
    private JTextField nom, prenom;
    private JButton saveButton;
    private JButton resetButton;


    IBCMainPanel(){
        this.setLayout(null);
        this.setBackground(Color.WHITE);
        this.setFocusable(true);

        title = new JLabel("IBC Client Application");
        title.setBounds(15, 20, 470, 60);
        title.setHorizontalAlignment(SwingConstants.CENTER);
        Font font = BauhausFont.BAUHAUS;
        font.deriveFont(30f);
        title.setFont(font);
        this.add(title);
        JPanel namePan = new JPanel();
        namePan.setBackground(Color.WHITE);
        namePan.setBounds(new Rectangle(15, 100, 470, 150));
        nom = new JTextField();
        nom.setText(ClientApplication.getClient().getUser().getName());
        nom.setPreferredSize(new Dimension(270, 30));
        prenom = new JTextField();
        prenom.setText(ClientApplication.getClient().getUser().getFirstName());
        prenom.setPreferredSize(new Dimension(270, 30));
        namePan.setBorder(BorderFactory.createTitledBorder("Nom & Prénom"));
        nomLabel = new JLabel("Saisir un nom :");
        nomLabel.setPreferredSize(new Dimension(120, 30));
        prenomLabel = new JLabel("Saisir un prénom :");
        prenomLabel.setPreferredSize(new Dimension(120, 30));

        saveButton = new JButton("Sauvegarder");
        saveButton.setPreferredSize(new Dimension(200, 40));
        saveButton.addActionListener((actionEvent) -> {
            System.out.println("Sauvegarde de l'utilisateur : name=" + nom.getText() + ";firstname=" + prenom.getText());
            ClientApplication.getClient().getUser().setFirstName(prenom.getText());
            ClientApplication.getClient().getUser().setName(nom.getText());
        });
        resetButton = new JButton("Réinitialiser");
        resetButton.setPreferredSize(new Dimension(200, 40));
        resetButton.addActionListener((actionEvent) -> {
            for (IBCPacketType type : IBCPacketType.values()){
                System.out.println("Tickets initialisés à 3 pour " + type.name());
                ClientApplication.getClient().getUser().setTicket(type, 3);
            }
        });
        namePan.add(prenomLabel);
        namePan.add(prenom);
        namePan.add(nomLabel);
        namePan.add(nom);

        namePan.add(saveButton);
        namePan.add(resetButton);

        this.add(namePan);
    }

}
