package com.cegesoft.ibc.client.utils;

import javax.crypto.Cipher;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.UUID;

import static java.nio.charset.StandardCharsets.UTF_8;

public class RSA {

    private static PrivateKey CLIENT_PRIVATE;
    private static PublicKey SERVER_PUBLIC;

    static {
        try {
            CLIENT_PRIVATE = readPrivateKey("client-private.key");
            SERVER_PUBLIC = readPublicKey("com.cegesoft.ibc.server-public.key");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception{
        //createNewKeys();
        String s = "M:" + UUID.randomUUID().toString() + ":001A7DDA7113#";
        byte[] encrypt = encrypt(s);
        System.out.println(new String(encrypt));
        System.out.println(encrypt.length);
        String decrypt = new String(decrypt(encrypt));
        System.out.println(decrypt);
    }

    public static void createNewKeys() throws Exception{
        KeyPair client = buildKeyPair(1024);
        saveKeyPair(client, "client-public.key", "client-private.key");
        KeyPair server = buildKeyPair(1024);
        saveKeyPair(server, "com.cegesoft.ibc.server-public.key", "com.cegesoft.ibc.server-private.key");
    }


    public static void saveKeyPair(KeyPair keyPair, String s1, String s2) throws IOException {
        PrivateKey privateKey = keyPair.getPrivate();
        PublicKey publicKey = keyPair.getPublic();

        // Store Public Key.
        X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(
                publicKey.getEncoded());
        FileOutputStream fos = new FileOutputStream(s1);
        fos.write(x509EncodedKeySpec.getEncoded());
        fos.close();

        // Store Private Key.
        PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(
                privateKey.getEncoded());
        fos = new FileOutputStream( s2);
        fos.write(pkcs8EncodedKeySpec.getEncoded());
        fos.close();
    }

    public static KeyPair buildKeyPair(final int keySize) throws NoSuchAlgorithmException {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(keySize);
        return keyPairGenerator.genKeyPair();
    }

    private static PrivateKey readPrivateKey(String filename) throws Exception {
        byte[] keyBytes = Files.readAllBytes(Paths.get(filename));
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }

    private static PublicKey readPublicKey(String filename) throws Exception {

        byte[] keyBytes = Files.readAllBytes(Paths.get(filename));
        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(spec);
    }

    public static byte[] encrypt(String message) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, CLIENT_PRIVATE);
        return cipher.doFinal(message.getBytes(UTF_8));
    }

    public static byte[] decrypt(byte[] encrypted) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, SERVER_PUBLIC);

        return cipher.doFinal(encrypted);
    }
}
