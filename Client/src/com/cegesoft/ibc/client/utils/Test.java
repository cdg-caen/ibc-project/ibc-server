package com.cegesoft.ibc.client.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Test {
    private static final String STRING_KEY = "Salut je suis Clément le Dev OP";

    public static void main(String[] ar) throws Exception {
        byte[] encrypted = encrypt("Salut je suis Clément");
        System.out.println(new String(encrypted, UTF_8));

        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(STRING_KEY.getBytes(), "AES"));
        System.out.println(new String(cipher.doFinal(encrypted), UTF_8));
    }

    public static byte[] encrypt(String message) throws Exception {
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(STRING_KEY.getBytes(), "AES"));
        return cipher.doFinal(message.getBytes(UTF_8));
    }
}
