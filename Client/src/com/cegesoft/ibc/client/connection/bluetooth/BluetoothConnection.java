package com.cegesoft.ibc.client.connection.bluetooth;

import com.cegesoft.ibc.client.exceptions.InvalidStreamException;
import com.cegesoft.ibc.client.utils.RSA;

import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import static java.nio.charset.StandardCharsets.UTF_8;

public class BluetoothConnection {

    private final String fullAddress;
    private StreamConnection connection;
    private DataOutputStream outputStream;
    private DataInputStream inputStream;

    private boolean isConnected = false;

    public BluetoothConnection(String address) {
        this.fullAddress = "btspp://" + address + ":1;authenticate=false;encrypt=false;master=false";
    }

    public void connect() {
        this.connect(0);
    }

    private void connect(int i) {
        try {
            this.connection = (StreamConnection) Connector.open(this.fullAddress);
            this.isConnected = true;
        } catch (IOException e) {
            if (i < 50) {
                System.out.println("Erreur numéro " + i + " : " + e.getMessage());
                connect(i + 1);
                return;
            }
            System.out.println("Erreur de connexion : ");
            e.printStackTrace();
            this.isConnected = false;
        }
    }

    public boolean isConnected() {
        return this.isConnected;
    }

    private void openInputStream() throws InvalidStreamException {
        try {
            this.inputStream = this.connection.openDataInputStream();
        } catch (IOException e) {
            throw new InvalidStreamException("Can't open inputStream : ");
        }
    }

    private void openOutputStream() throws InvalidStreamException {
        try {
            this.outputStream = this.connection.openDataOutputStream();
        } catch (IOException e) {
            throw new InvalidStreamException("Can't open outputStream : ");
        }
    }

    public void disconnect() throws IOException {
        if (this.outputStream != null)
            this.outputStream.close();
        if (this.inputStream != null)
            this.inputStream.close();
        this.connection.close();
    }

    private void writeUTF(String s) throws IOException, InvalidStreamException {
        if (this.outputStream == null)
            this.openOutputStream();
        this.outputStream.writeUTF(s);
    }

    private String readUTF() throws IOException, InvalidStreamException {
        if (this.inputStream == null)
            this.openInputStream();
        return this.inputStream.readUTF();
    }

    public void write(String s){
        try {
            this.writeUTF(s);
        } catch (IOException e) {
            System.out.println("Can't write packet '" + s + "' : " + e);
        } catch (InvalidStreamException e) {
            e.printStackTrace();
        }
    }

    private void writeBytes(byte[] b) throws IOException, InvalidStreamException {
        if (this.outputStream == null)
            this.openOutputStream();
        this.outputStream.write(b);
    }

    public void writeCrypted(String s){
        try {
            this.writeBytes(RSA.encrypt(s));
        } catch (IOException e) {
            System.out.println("Can't write packet '" + s + "' : " + e);
        } catch (InvalidStreamException e) {
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Can't encrypt message : ");
            e.printStackTrace();
        }
    }

    public String read(){
        try {
            return this.readUTF();
        } catch (IOException e) {
            System.out.println("Can't read packet : " + e);
        } catch (InvalidStreamException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String readCrypted(){
        try {
            return new String(RSA.decrypt(this.readBytes()), UTF_8);
        } catch (IOException e){
            System.out.println("Can't read packet : " + e);
        } catch (InvalidStreamException e) {
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Can't decrypt the message :");
            e.printStackTrace();
        }
        return "";
    }

    private byte[] readBytes() throws IOException, InvalidStreamException {
        if (this.inputStream == null)
            this.openInputStream();
        byte[] b = new byte[128];
        this.inputStream.readFully(b);
        return b;
    }

    public boolean dataAvailable() throws IOException {
        if (this.inputStream == null)
            return false;
        return this.inputStream.available() > -1;
    }

}
