package com.cegesoft.ibc.client.connection.serial;

import com.cegesoft.ibc.client.ClientApplication;
import com.cegesoft.ibc.client.exceptions.InvalidStreamException;
import com.cegesoft.ibc.client.utils.RSA;
import gnu.io.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.TooManyListenersException;

public class SerialHandler implements SerialPortEventListener {

    private static final int TIMEOUT = 2000;

    private SerialPort port;
    private InputStream in;
    private OutputStream out;
    private String readData;

    public SerialHandler(){
        this("", 0);
    }

    public SerialHandler(String port, int baud) {
        if (connect(port, baud)) {
            if (initIOStream()) {
                initListener();
            }
        }
    }

    public static void printPorts() {
        Enumeration ports = CommPortIdentifier.getPortIdentifiers();

        while (ports.hasMoreElements()) {
            CommPortIdentifier curPort = (CommPortIdentifier) ports.nextElement();

            //get only serial ports
            //if (curPort.getPortType() == CommPortIdentifier.PORT_SERIAL) {
            System.out.println("Port : " + curPort.getName());
            //}
        }
    }

    private CommPortIdentifier getPortByName(String name) {
        Enumeration ports = CommPortIdentifier.getPortIdentifiers();

        while (ports.hasMoreElements()) {
            CommPortIdentifier curPort = (CommPortIdentifier) ports.nextElement();

            //get only serial ports
            if (curPort.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                if (name.equalsIgnoreCase(curPort.getName())) {
                    System.out.println("Selected port '" + curPort.getName() + "'");
                    return curPort;
                }
            }
        }

        return null;
    }

    private boolean connect(String port, int baud) {
        if (baud == 0)
            baud = 600;
        CommPort commPort = null;
        String selecPort = port;
        if (port == null || port.equals("")) {
            System.out.println(ClientApplication.getClient().getConfig());
            if (ClientApplication.getClient().getConfig().getPortName().equals("")) {
                System.out.println("WARN : Selected Port name is null.");
            }
            selecPort = ClientApplication.getClient().getConfig().getPortName();
        }

        System.out.println("Port selectionné : " + selecPort);
        CommPortIdentifier comm = null;
        while (comm == null) {
            comm = getPortByName(selecPort);
        }

        int i = 0;

        while (commPort == null) {
            try {
                commPort = comm.open("ApplicationIBC", TIMEOUT);
                ((SerialPort) commPort).setSerialPortParams(
                        baud,
                        SerialPort.DATABITS_8,
                        SerialPort.STOPBITS_1,
                        SerialPort.PARITY_NONE);
                System.out.println("Serial Port enabled");
                this.port = (SerialPort) commPort;
                return true;
            } catch (Exception e) {
                i++;
                if (i > 50) {
                    e.printStackTrace();
                    return false;
                }
            }
        }
        return false;
    }

    private boolean initIOStream() {
        try {
            in = port.getInputStream();
            out = port.getOutputStream();
            System.out.println("IOStreams initialized");
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private void initListener() {

        /*while (true){
            try {
                if (in.available() > -1){
                    System.out.println(in.read());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/

        try {
            port.addEventListener(this);
            port.notifyOnDataAvailable(true);
            System.out.println("Listening to " + port.getName());
        } catch (TooManyListenersException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        try {
            port.removeEventListener();
            port.close();
            in.close();
            out.close();
            System.out.println("Serial successfully disconnected");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void serialEvent(SerialPortEvent e) {
        if (e.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
            try {
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }


    public String read() {
        while (readData == null) {
            try {
                if (port == null || in == null)
                    continue;
                if (in.available() == 0)
                    continue;

                System.out.println("Serial Data Available");
                StringBuilder str = new StringBuilder();
                int data;
                while ((data = in.read()) > -1) {
                    str.append((char) data);
                    System.out.println((char) data);
                    if (data == '#') {
                        break;
                    }
                }
                readData = str.toString();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        String data = readData;
        readData = null;
        return data;
    }

    private void writeBytes(byte[] b) throws IOException {
        this.out.write(b);
    }

    public void writeCrypted(String s){
        try {
            this.writeBytes(RSA.encrypt(s));
        } catch (IOException e) {
            System.out.println("Can't write packet '" + s + "' : " + e);
        } catch (Exception e) {
            System.out.println("Can't encrypt message : ");
            e.printStackTrace();
        }
    }

}
