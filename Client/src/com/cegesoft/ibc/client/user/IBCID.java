package com.cegesoft.ibc.client.user;

import java.util.UUID;

public class IBCID {

    private String id;

    private IBCID(String id){
        this.id = id;
    }

    @Override
    public String toString() {
        return id;
    }

    public static IBCID create(){
        return new IBCID(UUID.randomUUID().toString());
    }

    public static IBCID parse(String string){
        return new IBCID(string);
    }

}
