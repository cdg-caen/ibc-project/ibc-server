package com.cegesoft.ibc.client.user;

import com.cegesoft.ibc.client.packets.IBCPacketType;

import java.util.HashMap;

public class IBCUser {

    private final HashMap<IBCPacketType, Integer> tickets = new HashMap<>();
    private String name;
    private String firstName;
    private final IBCID id;

    public IBCUser(IBCID id){
        this.id = id;
    }

    public String toString(IBCPacketType type) {
        return id.toString() + "@" + name + "@" + firstName + "@" + tickets.getOrDefault(type, 0);
    }

    public void setTicket(IBCPacketType type, int amount) {
        tickets.remove(type);
        tickets.put(type, amount);
    }

    public boolean authorized(IBCPacketType type){
        return this.tickets.getOrDefault(type, 0) > 0;
    }

    public void extractTicket(IBCPacketType type){
        if (!authorized(type))
            return;
        this.tickets.replace(type, this.tickets.get(type)-1);
    }

    public IBCID getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<IBCPacketType, Integer> getTickets(){
        return tickets;
    }

}
